# Create the documentation in HTML and PDF format
docs: docs-html docs-pdf
	
# Create the documentation in HTML format
docs-html: apidoc
	make -C docs/ html

# Create the documentation in PDF format
docs-pdf: apidoc
	make -C docs/ latexpdf

# Create the API files for the documentation
apidoc: clean-apidoc
	sphinx-apidoc -H "Reference / API" -M -o docs/api/ alucine/

# Delete the API files for the documentation
clean-apidoc:
	rm -rf docs/api/

# Delete the compiled documentation
clean: clean-apidoc
	rm -rf docs/_build/

# Install ALUCINE
install:
	pip install .

# Uninstall ALUCINE
uninstall:
	pip uninstall -y alucine

