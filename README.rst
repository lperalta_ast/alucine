What Is ALUCINE?
----------------

ALUCINE (a Spanish acronym that stands for "Ajuste de Líneas para Unidades de
Campo Integral de Nebulosas en Emisión") is a Python package designed for
emission-line fitting of astronomical data observed with integral field units.


Citation
--------

If you use ALUCINE for research presented in a publication, we recommend,
encourage and are grateful to receive a citation to the following paper:

**Our paper has recently accepted to be publish in A&A we will provide the details in the next days**

By now, you can use the reference to the preprint published in arXiv: `arXiv:2305.06366 <https://ui.adsabs.harvard.edu/abs/2023arXiv230506366P/abstract>`_


Support
-------

This code have been designed to be used using data from any integral field unit. However, we are aware that some tweaks could be needed for adapting data from instruments which does not follow the FITS standard. Also, we know that the documentation is not completely written and that some initial training could be useful for newbies in the code.

If you are interested in using the code, please contact us at lperalta@cab.inta-csic.es . We are happy to collaborate and promote the usage of the code, and we will invest time in improving the code and its documentation while we detect that it is useful for the astronomical community.


License
-------

ALUCINE is licensed under `version 3 of the GNU General Public License
<https://www.gnu.org/licenses/gpl-3.0.html>`_.


Logo
----

The logo used to decorate the Git repository and the documentation is a tribute
to the `ALUCINE comics
<https://www.tebeosfera.com/colecciones/alucine_1984_bruguera.html>`_ published
by `Editorial Bruguera <https://en.wikipedia.org/wiki/Editorial_Bruguera>`_.
This usage has been informed to `Penguin Random House
<https://penguinrandomhouse.com/>`_, and we are waiting to know their opinion.


Documentation
-------------

The documentation for each version is available under the directory ``docs`` of
the ALUCINE Git repository.

The file format used for this documentation is `ReStructuredText
<https://en.wikipedia.org/wiki/ReStructuredText>`_. Thanks to the documentation 
generator `Sphinx
<https://en.wikipedia.org/wiki/Sphinx_(documentation_generator)>`_, the files
can be easily converted to multitude of formats (such as a HTML website or a PDF
document).

The documentation for the latest version in the `master` branch of the ALUCINE
repository is automatically deployed at https://lperalta_ast.gitlab.io/alucine/
.

