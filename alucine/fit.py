#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ALUCINE: A set of tools for emission line fitting of astronomical IFU data
# Copyright (C) 2022  Luis Peralta de Arriba
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
``fit`` file
"""


import logging as _logging
import sys as _sys
from argparse import ArgumentParser as _ArgumentParser

import numpy as _np
import matplotlib.pyplot as _plt
from scipy.optimize import curve_fit as _curve_fit
from astropy.io import fits as _fits

from .models import get_model_from_name as _get_model_from_name
from .models.gaussian import GaussianWithBaseline as _GaussianWithBaseline
from .utils.io import read_control_file as _read_control_file
from .utils.io import write_multifitting_to_hdf5 as _write_multifitting_to_hdf5
from .utils.logging import add_logging_to_parser as _add_logging_to_parser
from .utils.logging import set_logging_config as _set_logging_config
from .utils.logging import set_warning_config as _set_warning_config
from .utils.wcs import remove_kwds_of_third_axis as _remove_kwds_of_third_axis


def get_parser(add_help=True):
    """
    Parser of ``fit`` file
    """

    parser = _ArgumentParser(add_help=add_help,
        description='ALUCINE step 4: performing fits in each cubelet')

    parser.add_argument('control_file',
                        help='a YAML file with the control instructions')
    
    _add_logging_to_parser(parser)
    
    parser.set_defaults(func=_main)

    return parser


def _update_bounds(component_bounds, fit_options, bound_type, index):

    updated_component_bounds = component_bounds.copy()

    if bound_type in fit_options.keys():
        if index in fit_options[bound_type].keys():
            new_bounds = fit_options[bound_type][index]

            assert len(new_bounds) == len(updated_component_bounds)

            for i, new_bound in enumerate(new_bounds):
                if new_bound is not None:
                    updated_component_bounds[i] = new_bound

    return updated_component_bounds


def _get_goodness_of_fit(observed_ydata, fit_ydata, num_fit_params, noise):
    
    chisq = _np.sum((observed_ydata - fit_ydata)**2 / noise**2)
    
    num_observations = len(observed_ydata)
    
    degree_of_freedom = num_observations - num_fit_params
    
    reduced_chisq = chisq / degree_of_freedom
    
    return reduced_chisq


def _multi_fit(xdata, ydata, noise=None, rf_resolution=None,
               max_num_components=1, fit_model=_GaussianWithBaseline(),
               fit_options={}, plot_fit=False, spaxel_id=None,
               cont_rf_window_list=None):

    # Assert the input values

    assert max_num_components >= 1
    
    # Create a dictionary for saving the results
    
    result = {'xdata': xdata, 'ydata': ydata, 'noise': noise,
              'max_num_components': max_num_components}

    # Guess whether bounds should be provided to the fit function

    if 'provide_bounds' in fit_options.keys():
        provide_bounds = fit_options['provide_bounds']
    else:
        provide_bounds = True
    
    assert type(provide_bounds) is bool
    
    # Get bounds for the baseline

    baseline_low_bounds = fit_model.get_baseline_low_bounds(
        xdata, ydata, noise=noise, cont_rf_window_list=cont_rf_window_list)
    baseline_high_bounds = fit_model.get_baseline_high_bounds(
        xdata, ydata, noise=noise, cont_rf_window_list=cont_rf_window_list)

    # Get bounds for the component

    default_component_low_bounds = fit_model.get_component_low_bounds(
        xdata, ydata, noise=noise, rf_resolution=rf_resolution)
    default_component_high_bounds = fit_model.get_component_high_bounds(
        xdata, ydata, noise=noise, rf_resolution=rf_resolution)

    default_component_low_bounds = _update_bounds(
        default_component_low_bounds, fit_options, 'low_bounds', 'default')
    default_component_high_bounds = _update_bounds(
        default_component_high_bounds, fit_options, 'high_bounds', 'default')

    # Get initial parameters for the baseline

    baseline_init_params = fit_model.get_baseline_init_params(
        xdata, ydata, baseline_low_bounds, baseline_high_bounds)

    # Get data cleaned of NaNs

    clean_mask = _np.logical_not(_np.isnan(xdata) + _np.isnan(ydata))
    clean_xdata = xdata[clean_mask]
    clean_ydata = ydata[clean_mask]

    # Initialise the "previous" values in the first iteration

    init_params = baseline_init_params[:]
    fit_params = baseline_init_params[:]
    low_bounds = baseline_low_bounds[:]
    high_bounds = baseline_high_bounds[:]

    residual_data = ydata
    fit_error = 0

    # Check if the data are full of nan

    if len(clean_ydata) == 0:
        fit_error = -1
    
    # Fit the data
    
    for j in range(1, max_num_components + 1):

        if fit_error == 0:

            # Guess the peak to fit

            peak_to_fit = _np.nanmax(residual_data)

            # Get the component bounds

            component_low_bounds = _update_bounds(
                default_component_low_bounds, fit_options, 'low_bounds', j)
            component_high_bounds = _update_bounds(
                default_component_high_bounds, fit_options, 'high_bounds', j)

            # Set the initial guess for the parameters of the fit

            component_init_params = fit_model.get_component_init_params(
                xdata, residual_data, low_bounds=component_low_bounds,
                high_bounds=component_high_bounds)

            init_params = _np.concatenate([fit_params, component_init_params])

            # Evaluate the function at the initial parameters

            init_ydata = fit_model.fit_function(xdata, *init_params)

            # Set the bounds for the parameters

            low_bounds = _np.concatenate([low_bounds, component_low_bounds])
            high_bounds = _np.concatenate([high_bounds, component_high_bounds])

            bounds = _np.array([low_bounds, high_bounds])

            # Try to perform the fit (and get some values which depend on it)

            try:

                if provide_bounds is True:
                    popt, pcov = _curve_fit(
                        fit_model.fit_function, clean_xdata, clean_ydata,
                        p0=tuple(init_params), bounds=bounds)
                else:
                    popt, pcov = _curve_fit(
                        fit_model.fit_function, clean_xdata, clean_ydata,
                        p0=tuple(init_params))

                fit_params = popt
                fit_ydata = fit_model.fit_function(xdata, *fit_params)

                residual_data = ydata - fit_ydata

                goodness_of_fit = _get_goodness_of_fit(
                    ydata, fit_ydata, len(fit_params), noise)

            except:

                fit_error = 1

                fit_params = _np.nan * init_params
                fit_ydata = _np.nan * xdata

                residual_data = _np.nan * xdata

                goodness_of_fit = _np.nan

        else:

            fit_error = 2

            num_params = fit_model.get_num_params(j)

            peak_to_fit = _np.nan
            init_params = _np.nan * _np.zeros(num_params)
            init_ydata = _np.nan * xdata
            fit_params = _np.nan * _np.zeros(num_params)
            fit_ydata = _np.nan * xdata
            residual_data = _np.nan * xdata
            goodness_of_fit = _np.nan

        # Save the results of the fit

        result[j] = {}

        result[j]['fit_error'] = fit_error
        result[j]['peak_to_fit'] = peak_to_fit
        result[j]['init_params'] = init_params
        result[j]['init_ydata'] = init_ydata
        result[j]['fit_params'] = fit_params
        result[j]['fit_ydata'] = fit_ydata
        result[j]['residual_data'] = residual_data
        result[j]['goodness_of_fit'] = goodness_of_fit

        if plot_fit is True:

            fig, ax = _plt.subplots(figsize=[1.5 * 6.4, 4.8],
                                   gridspec_kw={'right': 0.75})
            
            ax.plot(xdata, ydata, label='spec')
            ax.plot(xdata, init_ydata, ls=':', lw=2, label='init')
            ax.plot(xdata, fit_ydata, ls='--', label='fit')
            ax.plot(xdata, residual_data, ls='-.', label='residual')

            text = ('num_components: {}\n'.format(j) +
                    'error: {}\n'.format(fit_error) +
                    'peak: {:.4g}\n'.format(peak_to_fit) +
                    'goodness: {:.4g}\n'.format(goodness_of_fit))

            text += 'init / fit:\n'
            for init_value, fit_value in zip(init_params, fit_params):
                text += '    {:.4g} / {:.4g}\n'.format(init_value, fit_value)

            text += 'bounds:\n'
            for init_value, fit_value in zip(low_bounds, high_bounds):
                text += '    {:.4g} / {:.4g}\n'.format(init_value, fit_value)

            ax.text(1.01, 1.0, text, transform=ax.transAxes, va='top')

            ax.legend()
            
            if spaxel_id is not None:
                ax.set_title('{}'.format(spaxel_id))

            _plt.show()

    return result


def _multi_fit_cubelet(
        x_vect, cubelet_array, noise_image, rf_resolution, max_num_components,
        fit_model=_GaussianWithBaseline(), fit_options={},
        cont_rf_window_list=None):

    num_pix_wl, num_pix_y, num_pix_x = cubelet_array.shape

    multi_fit_cubelet_result = {}

    for i in range(num_pix_x):
        
        for j in range(num_pix_y):
            
            y_vect = cubelet_array[:, j, i]
            noise = noise_image[j, i]
            
            fit_spaxel_result = _multi_fit(
                x_vect, y_vect, noise=noise, rf_resolution=rf_resolution,
                max_num_components=max_num_components, fit_model=fit_model,
                fit_options=fit_options, spaxel_id=(i, j),
                cont_rf_window_list=cont_rf_window_list)
            
            multi_fit_cubelet_result[(j, i)] = fit_spaxel_result
    
    return multi_fit_cubelet_result


def _fit_one_cubelet(
        cubelet_filename, redshift, rf_resolution, max_num_components=1,
        fit_model=_GaussianWithBaseline(), fit_options={},
        cont_rf_window_list=None):

    # Create a spectral cube from a FITS file

    with _fits.open(cubelet_filename, mode='readonly') as hdu_list:
        hdr = hdu_list[0].header.copy()
        cubelet_array = hdu_list[0].data
        noise_image = hdu_list['CONT_NOISE'].data
    
    # Get the wavelenght solution (observed and rest-frame)
    
    obs_wl_vect = (
        hdr['CRVAL3'] +
        hdr['CDELT3'] * (_np.arange(hdr['NAXIS3']) + 1 - hdr['CRPIX3']))

    rf_wl_vect = obs_wl_vect / (1.0 + redshift)

    # Fit the cubelet

    multi_fit_cubelet_result = _multi_fit_cubelet(
        rf_wl_vect, cubelet_array, noise_image, rf_resolution,
        max_num_components, fit_model=fit_model, fit_options=fit_options,
        cont_rf_window_list=cont_rf_window_list)
    
    return multi_fit_cubelet_result


def _sort_1narrower2(fit_params, num_components, fit_model):

    if (num_components >= 2):

        if ((fit_model.__name__ == 'GaussianWithBaseline') or
            (fit_model.__name__ == 'OiiiDoubletGaussianWithBaseline')):

            if fit_params[3] > fit_params[6]:
                former_first_component = fit_params[1:4].copy()
                former_second_component = fit_params[4:7].copy()

                fit_params[1:4] = former_second_component
                fit_params[4:7] = former_first_component

        elif fit_model.__name__ == 'SiiDoubletGaussianWithBaseline':

            if fit_params[3] > fit_params[7]:
                former_first_component = fit_params[1:5].copy()
                former_second_component = fit_params[5:9].copy()

                fit_params[1:5] = former_second_component
                fit_params[5:9] = former_first_component

        elif fit_model.__name__ == 'HalphaNiiGaussianWithBaseline':

            if fit_params[3] > fit_params[9]:
                former_first_sub_component = fit_params[1:4].copy()
                former_second_sub_component = fit_params[7:10].copy()

                fit_params[1:4] = former_second_sub_component
                fit_params[7:10] = former_first_sub_component

            if fit_params[6] > fit_params[12]:
                former_first_sub_component = fit_params[4:7].copy()
                former_second_sub_component = fit_params[10:13].copy()

                fit_params[4:7] = former_second_sub_component
                fit_params[10:13] = former_first_sub_component

        else:
            raise NotImplementedError

    return fit_params

        
def _sort_1narrower2_tuned_for_ngc2110(fit_params, num_components,
                                       fit_model, spaxel_key):

    if (num_components >= 2):

        if ((fit_model.__name__ == 'GaussianWithBaseline') or
            (fit_model.__name__ == 'OiiiDoubletGaussianWithBaseline')):

            if fit_params[3] > fit_params[6]:
                former_first_component = fit_params[1:4].copy()
                former_second_component = fit_params[4:7].copy()

                fit_params[1:4] = former_second_component
                fit_params[4:7] = former_first_component

        elif fit_model.__name__ == 'SiiDoubletGaussianWithBaseline':

            if fit_params[3] > fit_params[7]:
                former_first_component = fit_params[1:5].copy()
                former_second_component = fit_params[5:9].copy()

                fit_params[1:5] = former_second_component
                fit_params[5:9] = former_first_component

        elif fit_model.__name__ == 'HalphaNiiGaussianWithBaseline':

            if fit_params[3] > fit_params[9]:
                former_first_sub_component = fit_params[1:4].copy()
                former_second_sub_component = fit_params[7:10].copy()

                fit_params[1:4] = former_second_sub_component
                fit_params[7:10] = former_first_sub_component

            if fit_params[6] > fit_params[12]:
                former_first_sub_component = fit_params[4:7].copy()
                former_second_sub_component = fit_params[10:13].copy()

                fit_params[4:7] = former_second_sub_component
                fit_params[10:13] = former_first_sub_component

    if (num_components >= 2):

        y_coord, x_coord = spaxel_key

        max_x_coord, max_y_coord = 27.153333333333332, 20.5

        if y_coord > max_y_coord + 4:
            if ((fit_model.__name__ == 'GaussianWithBaseline') or
                (fit_model.__name__ == 'OiiiDoubletGaussianWithBaseline')):

                if fit_params[2] < fit_params[5]:
                    former_first_component = fit_params[1:4].copy()
                    former_second_component = fit_params[4:7].copy()

                    fit_params[1:4] = former_second_component
                    fit_params[4:7] = former_first_component

            elif fit_model.__name__ == 'SiiDoubletGaussianWithBaseline':

                if fit_params[2] < fit_params[6]:
                    former_first_component = fit_params[1:5].copy()
                    former_second_component = fit_params[5:9].copy()

                    fit_params[1:5] = former_second_component
                    fit_params[5:9] = former_first_component

            elif fit_model.__name__ == 'HalphaNiiGaussianWithBaseline':

                if fit_params[2] < fit_params[8]:
                    former_first_sub_component = fit_params[1:4].copy()
                    former_second_sub_component = fit_params[7:10].copy()

                    fit_params[1:4] = former_second_sub_component
                    fit_params[7:10] = former_first_sub_component

                if fit_params[5] < fit_params[11]:
                    former_first_sub_component = fit_params[4:7].copy()
                    former_second_sub_component = fit_params[10:13].copy()

                    fit_params[4:7] = former_second_sub_component
                    fit_params[10:13] = former_first_sub_component

    return fit_params

        
def _sort_1narrower2_tuned_for_ngc2110_hbeta(
        fit_params, num_components, fit_model, spaxel_key,
        line_rf_wl, rf_resolution):

    if (num_components >= 2):

        if ((fit_model.__name__ == 'GaussianWithBaseline') or
            (fit_model.__name__ == 'OiiiDoubletGaussianWithBaseline')):

            if fit_params[3] > fit_params[6]:

                former_first_component = fit_params[1:4].copy()
                former_second_component = fit_params[4:7].copy()

                fit_params[1:4] = former_second_component
                fit_params[4:7] = former_first_component

        elif fit_model.__name__ == 'SiiDoubletGaussianWithBaseline':

            if fit_params[3] > fit_params[7]:
                former_first_component = fit_params[1:5].copy()
                former_second_component = fit_params[5:9].copy()

                fit_params[1:5] = former_second_component
                fit_params[5:9] = former_first_component

        elif fit_model.__name__ == 'HalphaNiiGaussianWithBaseline':

            if fit_params[3] > fit_params[9]:
                former_first_sub_component = fit_params[1:4].copy()
                former_second_sub_component = fit_params[7:10].copy()

                fit_params[1:4] = former_second_sub_component
                fit_params[7:10] = former_first_sub_component

            if fit_params[6] > fit_params[12]:
                former_first_sub_component = fit_params[4:7].copy()
                former_second_sub_component = fit_params[10:13].copy()

                fit_params[4:7] = former_second_sub_component
                fit_params[10:13] = former_first_sub_component

    if (num_components >= 2):

        y_coord, x_coord = spaxel_key

        fit_params_cube = _np.reshape(fit_params, (fit_params.shape[0], 1, 1))
        sci_params_cube = fit_model.fit_to_sci_params_cube(
            fit_params_cube, line_rf_wl, rf_resolution)
        sci_params = sci_params_cube[:, 0, 0]

        condition = (
            ((_np.abs(sci_params[3] - sci_params[6]) < 90) &
                (_np.abs(sci_params[2]) > _np.abs(sci_params[5])) & 
                (y_coord > 16))
        )

        if condition == True:
            if ((fit_model.__name__ == 'GaussianWithBaseline') or
                (fit_model.__name__ == 'OiiiDoubletGaussianWithBaseline')):

                    former_first_component = fit_params[1:4].copy()
                    former_second_component = fit_params[4:7].copy()

                    fit_params[1:4] = former_second_component
                    fit_params[4:7] = former_first_component

    return fit_params

        
def _sort_1narrower2_tuned_for_ngc2110_oiii(
        fit_params, num_components, fit_model, spaxel_key,
        line_rf_wl, rf_resolution):

    if (num_components >= 2):

        if ((fit_model.__name__ == 'GaussianWithBaseline') or
            (fit_model.__name__ == 'OiiiDoubletGaussianWithBaseline')):

            if fit_params[3] > fit_params[6]:

                former_first_component = fit_params[1:4].copy()
                former_second_component = fit_params[4:7].copy()

                fit_params[1:4] = former_second_component
                fit_params[4:7] = former_first_component

        elif fit_model.__name__ == 'SiiDoubletGaussianWithBaseline':

            if fit_params[3] > fit_params[7]:
                former_first_component = fit_params[1:5].copy()
                former_second_component = fit_params[5:9].copy()

                fit_params[1:5] = former_second_component
                fit_params[5:9] = former_first_component

        elif fit_model.__name__ == 'HalphaNiiGaussianWithBaseline':

            if fit_params[3] > fit_params[9]:
                former_first_sub_component = fit_params[1:4].copy()
                former_second_sub_component = fit_params[7:10].copy()

                fit_params[1:4] = former_second_sub_component
                fit_params[7:10] = former_first_sub_component

            if fit_params[6] > fit_params[12]:
                former_first_sub_component = fit_params[4:7].copy()
                former_second_sub_component = fit_params[10:13].copy()

                fit_params[4:7] = former_second_sub_component
                fit_params[10:13] = former_first_sub_component

    if (num_components >= 2):

        y_coord, x_coord = spaxel_key

        fit_params_cube = _np.reshape(fit_params, (fit_params.shape[0], 1, 1))
        sci_params_cube = fit_model.fit_to_sci_params_cube(
            fit_params_cube, line_rf_wl, rf_resolution)
        sci_params = sci_params_cube[:, 0, 0]

        condition = (
            ((_np.abs(sci_params[3] - sci_params[6]) < 90) &
                (_np.abs(sci_params[2]) > _np.abs(sci_params[5])) & 
                (y_coord > 16))
        )

        if condition == True:
            if ((fit_model.__name__ == 'GaussianWithBaseline') or
                (fit_model.__name__ == 'OiiiDoubletGaussianWithBaseline')):

                    former_first_component = fit_params[1:4].copy()
                    former_second_component = fit_params[4:7].copy()

                    fit_params[1:4] = former_second_component
                    fit_params[4:7] = former_first_component

    return fit_params

        
def _sort_1narrower2_tuned_for_ngc2110_halphanii(
        fit_params, num_components, fit_model, spaxel_key,
        line_rf_wl, rf_resolution):

    if (num_components >= 2):

        if ((fit_model.__name__ == 'GaussianWithBaseline') or
            (fit_model.__name__ == 'OiiiDoubletGaussianWithBaseline')):

            if fit_params[3] > fit_params[6]:
                former_first_component = fit_params[1:4].copy()
                former_second_component = fit_params[4:7].copy()

                fit_params[1:4] = former_second_component
                fit_params[4:7] = former_first_component

        elif fit_model.__name__ == 'SiiDoubletGaussianWithBaseline':

            if fit_params[3] > fit_params[7]:
                former_first_component = fit_params[1:5].copy()
                former_second_component = fit_params[5:9].copy()

                fit_params[1:5] = former_second_component
                fit_params[5:9] = former_first_component

        elif fit_model.__name__ == 'HalphaNiiGaussianWithBaseline':

            if fit_params[3] > fit_params[9]:
                former_first_sub_component = fit_params[1:4].copy()
                former_second_sub_component = fit_params[7:10].copy()

                fit_params[1:4] = former_second_sub_component
                fit_params[7:10] = former_first_sub_component

            if fit_params[6] > fit_params[12]:
                former_first_sub_component = fit_params[4:7].copy()
                former_second_sub_component = fit_params[10:13].copy()

                fit_params[4:7] = former_second_sub_component
                fit_params[10:13] = former_first_sub_component

    if (num_components >= 2):

        y_coord, x_coord = spaxel_key

        fit_params_cube = _np.reshape(fit_params, (fit_params.shape[0], 1, 1))
        sci_params_cube = fit_model.fit_to_sci_params_cube(
            fit_params_cube, line_rf_wl, rf_resolution)
        sci_params = sci_params_cube[:, 0, 0]

        condition = (
            ((_np.abs(sci_params[3] - sci_params[9]) < 90) &
                (_np.abs(sci_params[2]) > _np.abs(sci_params[8])) & 
                (x_coord > 24) & (x_coord < 38) & (y_coord > 26)) |
                ( (x_coord, y_coord) in [
                (15, 12), (21, 31), (23, 32), (23, 33), (24, 33), (22, 37),
                (22, 38), (23, 37), (23, 38), (37, 31)])
        )

        if condition == True:
            if fit_model.__name__ == 'HalphaNiiGaussianWithBaseline':

                    former_first_sub_component = fit_params[1:4].copy()
                    former_second_sub_component = fit_params[7:10].copy()

                    fit_params[1:4] = former_second_sub_component
                    fit_params[7:10] = former_first_sub_component

    if (num_components >= 2):

        y_coord, x_coord = spaxel_key

        fit_params_cube = _np.reshape(fit_params, (fit_params.shape[0], 1, 1))
        sci_params_cube = fit_model.fit_to_sci_params_cube(
            fit_params_cube, line_rf_wl, rf_resolution)
        sci_params = sci_params_cube[:, 0, 0]

        condition = (
            ((_np.abs(sci_params[6] - sci_params[12]) < 90) &
                (_np.abs(sci_params[5]) > _np.abs(sci_params[11])) & 
                (x_coord > 24) & (x_coord < 38) & (y_coord > 28)) |
                ( (x_coord, y_coord) in [(22, 38)])
        )

        if condition == True:
            if fit_model.__name__ == 'HalphaNiiGaussianWithBaseline':

                    former_first_sub_component = fit_params[4:7].copy()
                    former_second_sub_component = fit_params[10:13].copy()

                    fit_params[4:7] = former_second_sub_component
                    fit_params[10:13] = former_first_sub_component

    return fit_params

        
def _sort_1narrower2_tuned_for_ngc2110_sii(
        fit_params, num_components, fit_model, spaxel_key,
        line_rf_wl, rf_resolution):

    if (num_components >= 2):

        if ((fit_model.__name__ == 'GaussianWithBaseline') or
            (fit_model.__name__ == 'OiiiDoubletGaussianWithBaseline')):

            if fit_params[3] > fit_params[6]:
                former_first_component = fit_params[1:4].copy()
                former_second_component = fit_params[4:7].copy()

                fit_params[1:4] = former_second_component
                fit_params[4:7] = former_first_component

        elif fit_model.__name__ == 'SiiDoubletGaussianWithBaseline':

            if fit_params[3] > fit_params[7]:
                former_first_component = fit_params[1:5].copy()
                former_second_component = fit_params[5:9].copy()

                fit_params[1:5] = former_second_component
                fit_params[5:9] = former_first_component

        elif fit_model.__name__ == 'HalphaNiiGaussianWithBaseline':

            if fit_params[3] > fit_params[9]:
                former_first_sub_component = fit_params[1:4].copy()
                former_second_sub_component = fit_params[7:10].copy()

                fit_params[1:4] = former_second_sub_component
                fit_params[7:10] = former_first_sub_component

            if fit_params[6] > fit_params[12]:
                former_first_sub_component = fit_params[4:7].copy()
                former_second_sub_component = fit_params[10:13].copy()

                fit_params[4:7] = former_second_sub_component
                fit_params[10:13] = former_first_sub_component

    if (num_components >= 2):

        y_coord, x_coord = spaxel_key

        fit_params_cube = _np.reshape(fit_params, (fit_params.shape[0], 1, 1))
        sci_params_cube = fit_model.fit_to_sci_params_cube(
            fit_params_cube, line_rf_wl, rf_resolution)
        sci_params = sci_params_cube[:, 0, 0]

        condition = (
            ((_np.abs(sci_params[3] - sci_params[7]) < 90) &
                (_np.abs(sci_params[2]) > _np.abs(sci_params[6])) & 
                (x_coord > 25) & (x_coord < 36) & (y_coord > 27)) |
                ( (x_coord, y_coord) in [
                (22, 37), (23, 35), (24, 36), (23, 33), (24, 33), (24, 33),
                (25, 33), (25, 32), (25, 36), (25, 35), (25, 36), (36, 34),
                (36, 33)])
        )

        if condition == True:

            if fit_model.__name__ == 'SiiDoubletGaussianWithBaseline':

                    former_first_component = fit_params[1:5].copy()
                    former_second_component = fit_params[5:9].copy()

                    fit_params[1:5] = former_second_component
                    fit_params[5:9] = former_first_component

    return fit_params

        
def _sort_1narrower2_tuned_for_ngc2110_oi(
        fit_params, num_components, fit_model, spaxel_key,
        line_rf_wl, rf_resolution):

    if (num_components >= 2):

        if ((fit_model.__name__ == 'GaussianWithBaseline') or
            (fit_model.__name__ == 'OiiiDoubletGaussianWithBaseline')):

            if fit_params[3] > fit_params[6]:
                former_first_component = fit_params[1:4].copy()
                former_second_component = fit_params[4:7].copy()

                fit_params[1:4] = former_second_component
                fit_params[4:7] = former_first_component

        elif fit_model.__name__ == 'SiiDoubletGaussianWithBaseline':

            if fit_params[3] > fit_params[7]:
                former_first_component = fit_params[1:5].copy()
                former_second_component = fit_params[5:9].copy()

                fit_params[1:5] = former_second_component
                fit_params[5:9] = former_first_component

        elif fit_model.__name__ == 'HalphaNiiGaussianWithBaseline':

            if fit_params[3] > fit_params[9]:
                former_first_sub_component = fit_params[1:4].copy()
                former_second_sub_component = fit_params[7:10].copy()

                fit_params[1:4] = former_second_sub_component
                fit_params[7:10] = former_first_sub_component

            if fit_params[6] > fit_params[12]:
                former_first_sub_component = fit_params[4:7].copy()
                former_second_sub_component = fit_params[10:13].copy()

                fit_params[4:7] = former_second_sub_component
                fit_params[10:13] = former_first_sub_component

    if (num_components >= 2):

        y_coord, x_coord = spaxel_key

        fit_params_cube = _np.reshape(fit_params, (fit_params.shape[0], 1, 1))
        sci_params_cube = fit_model.fit_to_sci_params_cube(
            fit_params_cube, line_rf_wl, rf_resolution)
        sci_params = sci_params_cube[:, 0, 0]

        condition = (
            ((_np.abs(sci_params[3] - sci_params[6]) < 90) &
                (_np.abs(sci_params[2]) > _np.abs(sci_params[5])) &
                (y_coord > 16))
        )

        if condition == True:
            if ((fit_model.__name__ == 'GaussianWithBaseline') or
                (fit_model.__name__ == 'OiiiDoubletGaussianWithBaseline')):

                    former_first_component = fit_params[1:4].copy()
                    former_second_component = fit_params[4:7].copy()

                    fit_params[1:4] = former_second_component
                    fit_params[4:7] = former_first_component

    return fit_params

        
def _sort_1narrower2narrower3(fit_params, num_components, fit_model):

    if (num_components >= 2):

        if ((fit_model.__name__ == 'GaussianWithBaseline') or
            (fit_model.__name__ == 'OiiiDoubletGaussianWithBaseline')):

            if fit_params[3] > fit_params[6]:
                former_first_component = fit_params[1:4].copy()
                former_second_component = fit_params[4:7].copy()

                fit_params[1:4] = former_second_component
                fit_params[4:7] = former_first_component

        elif fit_model.__name__ == 'SiiDoubletGaussianWithBaseline':

            if fit_params[3] > fit_params[7]:
                former_first_component = fit_params[1:5].copy()
                former_second_component = fit_params[5:9].copy()

                fit_params[1:5] = former_second_component
                fit_params[5:9] = former_first_component

        elif fit_model.__name__ == 'HalphaNiiGaussianWithBaseline':

            if fit_params[3] > fit_params[9]:
                former_first_sub_component = fit_params[1:4].copy()
                former_second_sub_component = fit_params[7:10].copy()

                fit_params[1:4] = former_second_sub_component
                fit_params[7:10] = former_first_sub_component

            if fit_params[6] > fit_params[12]:
                former_first_sub_component = fit_params[4:7].copy()
                former_second_sub_component = fit_params[10:13].copy()

                fit_params[4:7] = former_second_sub_component
                fit_params[10:13] = former_first_sub_component

        else:
            raise NotImplementedError

    return fit_params


def _sort_two_with_1smallest_2biggest(
    former_first_width, former_second_width,
    former_first_component, former_second_component):
    
    if former_first_width < former_second_width:
        new_first_component = former_first_component
        new_second_component = former_second_component
    else:
        new_first_component = former_second_component
        new_second_component = former_first_component
    
    return new_first_component, new_second_component


def _sort_three_with_1smallest_2biggest(
    former_first_width, former_second_width, former_third_width,
    former_first_component, former_second_component, former_third_component):
    
    if ((former_first_width <= former_second_width) and
        (former_second_width <= former_third_width)):
        new_first_component = former_first_component
        new_second_component = former_third_component
        new_third_component = former_second_component
    elif ((former_first_width <= former_third_width) and
          (former_third_width <= former_second_width)):
        new_first_component = former_first_component
        new_second_component = former_second_component
        new_third_component = former_third_component
    elif ((former_second_width <= former_first_width) and
          (former_first_width <= former_third_width)):
        new_first_component = former_second_component
        new_second_component = former_third_component
        new_third_component = former_first_component
    elif ((former_second_width <= former_third_width) and
          (former_third_width <= former_first_width)):
        new_first_component = former_second_component
        new_second_component = former_first_component
        new_third_component = former_third_component
    elif ((former_third_width <= former_first_width) and
          (former_first_width <= former_second_width)):
        new_first_component = former_third_component
        new_second_component = former_second_component
        new_third_component = former_first_component
    elif ((former_third_width <= former_second_width) and
          (former_second_width <= former_first_width)):
        new_first_component = former_third_component
        new_second_component = former_first_component
        new_third_component = former_second_component
    else:
        raise ValueError
    
    return new_first_component, new_second_component, new_third_component


def _sort_1narrowest2broadest3middle(fit_params, num_components, fit_model):

    if (num_components == 0) or (num_components == 1):
        pass
    elif (num_components == 2):

        if ((fit_model.__name__ == 'GaussianWithBaseline') or
            (fit_model.__name__ == 'OiiiDoubletGaussianWithBaseline')):
                
            former_first_width = fit_params[3]
            former_second_width = fit_params[6]
            
            former_first_component = fit_params[1:4].copy()
            former_second_component = fit_params[4:7].copy()
            
            new_first_component, new_second_component = _sort_two_with_1smallest_2biggest(
                former_first_width, former_second_width,
                former_first_component, former_second_component)

            fit_params[1:4] = new_first_component
            fit_params[4:7] = new_second_component

        elif fit_model.__name__ == 'SiiDoubletGaussianWithBaseline':
                
            former_first_width = fit_params[3]
            former_second_width = fit_params[7]
            
            former_first_component = fit_params[1:5].copy()
            former_second_component = fit_params[5:9].copy()
            
            new_first_component, new_second_component = _sort_two_with_1smallest_2biggest(
                former_first_width, former_second_width,
                former_first_component, former_second_component)

            fit_params[1:5] = new_first_component
            fit_params[5:9] = new_second_component

        elif fit_model.__name__ == 'HalphaNiiGaussianWithBaseline':
            
            # First subcomponent
            
            former_first_width = fit_params[3]
            former_second_width = fit_params[9]
            
            former_first_component = fit_params[1:4].copy()
            former_second_component = fit_params[7:10].copy()
            
            new_first_component, new_second_component = _sort_two_with_1smallest_2biggest(
                former_first_width, former_second_width,
                former_first_component, former_second_component)

            fit_params[1:4] = new_first_component
            fit_params[7:10] = new_second_component
            
            # Second subcomponent
            
            former_first_width = fit_params[6]
            former_second_width = fit_params[12]
            
            former_first_component = fit_params[4:7].copy()
            former_second_component = fit_params[10:13].copy()
            
            new_first_component, new_second_component = _sort_two_with_1smallest_2biggest(
                former_first_width, former_second_width,
                former_first_component, former_second_component)

            fit_params[4:7] = new_first_component
            fit_params[10:13] = new_second_component

        else:
            raise NotImplementedError

    elif (num_components == 3):

        if ((fit_model.__name__ == 'GaussianWithBaseline') or
            (fit_model.__name__ == 'OiiiDoubletGaussianWithBaseline')):
                
            former_first_width = fit_params[3]
            former_second_width = fit_params[6]
            former_third_width = fit_params[9]
            
            former_first_component = fit_params[1:4].copy()
            former_second_component = fit_params[4:7].copy()
            former_third_component = fit_params[7:10].copy()
            
            new_first_component, new_second_component, new_third_component = _sort_three_with_1smallest_2biggest(
                former_first_width, former_second_width, former_third_width,
                former_first_component, former_second_component, former_third_component)

            fit_params[1:4] = new_first_component
            fit_params[4:7] = new_second_component
            fit_params[7:10] = new_third_component

        elif fit_model.__name__ == 'SiiDoubletGaussianWithBaseline':
                
            former_first_width = fit_params[3]
            former_second_width = fit_params[7]
            former_third_width = fit_params[11]
            
            former_first_component = fit_params[1:5].copy()
            former_second_component = fit_params[5:9].copy()
            former_third_component = fit_params[9:13].copy()
            
            new_first_component, new_second_component, new_third_component = _sort_three_with_1smallest_2biggest(
                former_first_width, former_second_width, former_third_width,
                former_first_component, former_second_component, former_third_component)

            fit_params[1:5] = new_first_component
            fit_params[5:9] = new_second_component
            fit_params[9:13] = new_third_component

        elif fit_model.__name__ == 'HalphaNiiGaussianWithBaseline':
            
            # First subcomponent
            
            former_first_width = fit_params[3]
            former_second_width = fit_params[9]
            former_third_width = fit_params[15]
            
            former_first_component = fit_params[1:4].copy()
            former_second_component = fit_params[7:10].copy()
            former_third_component = fit_params[13:16].copy()
            
            new_first_component, new_second_component, new_third_component = _sort_three_with_1smallest_2biggest(
                former_first_width, former_second_width, former_third_width,
                former_first_component, former_second_component, former_third_component)

            fit_params[1:4] = new_first_component
            fit_params[7:10] = new_second_component
            fit_params[13:16] = new_third_component
            
            # Second subcomponent
            
            former_first_width = fit_params[6]
            former_second_width = fit_params[12]
            former_third_width = fit_params[18]
            
            former_first_component = fit_params[4:7].copy()
            former_second_component = fit_params[10:13].copy()
            former_third_component = fit_params[16:19].copy()
            
            new_first_component, new_second_component, new_third_component = _sort_three_with_1smallest_2biggest(
                former_first_width, former_second_width, former_third_width,
                former_first_component, former_second_component, former_third_component)

            fit_params[4:7] = new_first_component
            fit_params[10:13] = new_second_component
            fit_params[16:19] = new_third_component

        else:
            raise NotImplementedError

    else:
        raise ValueError

    return fit_params


def _sort_three_with_1middle_2biggest(
        former_first_width, former_second_width, former_third_width,
        former_first_component, former_second_component, former_third_component):
    
    if ((former_first_width <= former_second_width) and
        (former_second_width <= former_third_width)):
        new_first_component = former_second_component
        new_second_component = former_third_component
        new_third_component = former_first_component
    elif ((former_first_width <= former_third_width) and
          (former_third_width <= former_second_width)):
        new_first_component = former_third_component
        new_second_component = former_second_component
        new_third_component = former_first_component
    elif ((former_second_width <= former_first_width) and
          (former_first_width <= former_third_width)):
        new_first_component = former_first_component
        new_second_component = former_third_component
        new_third_component = former_second_component
    elif ((former_second_width <= former_third_width) and
          (former_third_width <= former_first_width)):
        new_first_component = former_third_component
        new_second_component = former_first_component
        new_third_component = former_second_component
    elif ((former_third_width <= former_first_width) and
          (former_first_width <= former_second_width)):
        new_first_component = former_first_component
        new_second_component = former_second_component
        new_third_component = former_third_component
    elif ((former_third_width <= former_second_width) and
          (former_second_width <= former_first_width)):
        new_first_component = former_second_component
        new_second_component = former_first_component
        new_third_component = former_third_component
    else:
        raise ValueError
    
    return new_first_component, new_second_component, new_third_component


def _sort_1middle2broadest3narrowest(fit_params, num_components, fit_model):

    if (num_components == 0) or (num_components == 1):
        pass
    elif (num_components == 2):

        if ((fit_model.__name__ == 'GaussianWithBaseline') or
            (fit_model.__name__ == 'OiiiDoubletGaussianWithBaseline')):
                
            former_first_width = fit_params[3]
            former_second_width = fit_params[6]
            
            former_first_component = fit_params[1:4].copy()
            former_second_component = fit_params[4:7].copy()
            
            new_first_component, new_second_component = _sort_two_with_1smallest_2biggest(
                former_first_width, former_second_width,
                former_first_component, former_second_component)

            fit_params[1:4] = new_first_component
            fit_params[4:7] = new_second_component

        elif fit_model.__name__ == 'SiiDoubletGaussianWithBaseline':
                
            former_first_width = fit_params[3]
            former_second_width = fit_params[7]
            
            former_first_component = fit_params[1:5].copy()
            former_second_component = fit_params[5:9].copy()
            
            new_first_component, new_second_component = _sort_two_with_1smallest_2biggest(
                former_first_width, former_second_width,
                former_first_component, former_second_component)

            fit_params[1:5] = new_first_component
            fit_params[5:9] = new_second_component

        elif fit_model.__name__ == 'HalphaNiiGaussianWithBaseline':
            
            # First subcomponent
            
            former_first_width = fit_params[3]
            former_second_width = fit_params[9]
            
            former_first_component = fit_params[1:4].copy()
            former_second_component = fit_params[7:10].copy()
            
            new_first_component, new_second_component = _sort_two_with_1smallest_2biggest(
                former_first_width, former_second_width,
                former_first_component, former_second_component)

            fit_params[1:4] = new_first_component
            fit_params[7:10] = new_second_component
            
            # Second subcomponent
            
            former_first_width = fit_params[6]
            former_second_width = fit_params[12]
            
            former_first_component = fit_params[4:7].copy()
            former_second_component = fit_params[10:13].copy()
            
            new_first_component, new_second_component = _sort_two_with_1smallest_2biggest(
                former_first_width, former_second_width,
                former_first_component, former_second_component)

            fit_params[4:7] = new_first_component
            fit_params[10:13] = new_second_component

        else:
            raise NotImplementedError

    elif (num_components == 3):

        if ((fit_model.__name__ == 'GaussianWithBaseline') or
            (fit_model.__name__ == 'OiiiDoubletGaussianWithBaseline')):
                
            former_first_width = fit_params[3]
            former_second_width = fit_params[6]
            former_third_width = fit_params[9]
            
            former_first_component = fit_params[1:4].copy()
            former_second_component = fit_params[4:7].copy()
            former_third_component = fit_params[7:10].copy()
            
            new_first_component, new_second_component, new_third_component = _sort_three_with_1middle_2biggest(
                former_first_width, former_second_width, former_third_width,
                former_first_component, former_second_component, former_third_component)

            fit_params[1:4] = new_first_component
            fit_params[4:7] = new_second_component
            fit_params[7:10] = new_third_component

        elif fit_model.__name__ == 'SiiDoubletGaussianWithBaseline':
                
            former_first_width = fit_params[3]
            former_second_width = fit_params[7]
            former_third_width = fit_params[11]
            
            former_first_component = fit_params[1:5].copy()
            former_second_component = fit_params[5:9].copy()
            former_third_component = fit_params[9:13].copy()
            
            new_first_component, new_second_component, new_third_component = _sort_three_with_1middle_2biggest(
                former_first_width, former_second_width, former_third_width,
                former_first_component, former_second_component, former_third_component)

            fit_params[1:5] = new_first_component
            fit_params[5:9] = new_second_component
            fit_params[9:13] = new_third_component

        elif fit_model.__name__ == 'HalphaNiiGaussianWithBaseline':
            
            # First subcomponent
            
            former_first_width = fit_params[3]
            former_second_width = fit_params[9]
            former_third_width = fit_params[15]
            
            former_first_component = fit_params[1:4].copy()
            former_second_component = fit_params[7:10].copy()
            former_third_component = fit_params[13:16].copy()
            
            new_first_component, new_second_component, new_third_component = _sort_three_with_1middle_2biggest(
                former_first_width, former_second_width, former_third_width,
                former_first_component, former_second_component, former_third_component)

            fit_params[1:4] = new_first_component
            fit_params[7:10] = new_second_component
            fit_params[13:16] = new_third_component
            
            # Second subcomponent
            
            former_first_width = fit_params[6]
            former_second_width = fit_params[12]
            former_third_width = fit_params[18]
            
            former_first_component = fit_params[4:7].copy()
            former_second_component = fit_params[10:13].copy()
            former_third_component = fit_params[16:19].copy()
            
            new_first_component, new_second_component, new_third_component = _sort_three_with_1middle_2biggest(
                former_first_width, former_second_width, former_third_width,
                former_first_component, former_second_component, former_third_component)

            fit_params[4:7] = new_first_component
            fit_params[10:13] = new_second_component
            fit_params[16:19] = new_third_component

        else:
            raise NotImplementedError

    else:
        raise ValueError

    return fit_params


def _sort_components_of_best_fit(fit_params, num_components, fit_model,
                                 best_fit_options, spaxel_key=None,
                                 line_rf_wl=None, rf_resolution=None):

    if (best_fit_options['sort_components'] == '1narrower2'):
        fit_params = _sort_1narrower2(
            fit_params, num_components, fit_model)

    if (best_fit_options['sort_components'] == '1narrower2_tuned_for_ngc2110'):
        fit_params = _sort_1narrower2_tuned_for_ngc2110(
            fit_params, num_components, fit_model, spaxel_key)

    if (best_fit_options['sort_components'] == '1narrower2_tuned_for_ngc2110_hbeta'):
        fit_params = _sort_1narrower2_tuned_for_ngc2110_hbeta(
            fit_params, num_components, fit_model, spaxel_key,
            line_rf_wl, rf_resolution)

    if (best_fit_options['sort_components'] == '1narrower2_tuned_for_ngc2110_oiii'):
        fit_params = _sort_1narrower2_tuned_for_ngc2110_oiii(
            fit_params, num_components, fit_model, spaxel_key,
            line_rf_wl, rf_resolution)

    if (best_fit_options['sort_components'] == '1narrower2_tuned_for_ngc2110_halphanii'):
        fit_params = _sort_1narrower2_tuned_for_ngc2110_halphanii(
            fit_params, num_components, fit_model, spaxel_key,
            line_rf_wl, rf_resolution)

    if (best_fit_options['sort_components'] == '1narrower2_tuned_for_ngc2110_sii'):
        fit_params = _sort_1narrower2_tuned_for_ngc2110_sii(
            fit_params, num_components, fit_model, spaxel_key,
            line_rf_wl, rf_resolution)

    if (best_fit_options['sort_components'] == '1narrower2_tuned_for_ngc2110_oi'):
        fit_params = _sort_1narrower2_tuned_for_ngc2110_oi(
            fit_params, num_components, fit_model, spaxel_key,
            line_rf_wl, rf_resolution)

    if (best_fit_options['sort_components'] == '1narrower2narrower3'):
        fit_params = _sort_1narrower2narrower3(
            fit_params, num_components, fit_model)

    if (best_fit_options['sort_components'] == '1narrowest2broadest3middle'):
        fit_params = _sort_1narrowest2broadest3middle(
            fit_params, num_components, fit_model)

    if (best_fit_options['sort_components'] == '1middle2broadest3narrowest'):
        fit_params = _sort_1middle2broadest3narrowest(
            fit_params, num_components, fit_model)

    return fit_params


def _get_best_fit_based_on_aon(multi_fit_cubelet_result, best_fit_options,
                               fit_model=_GaussianWithBaseline(),
                               line_rf_wl=None, rf_resolution=None):

    best_fit_result = {}

    for i, spaxel_key in enumerate(multi_fit_cubelet_result.keys()):
        
        best_fit_result[spaxel_key] = {}
        
        spaxel_result_dict = multi_fit_cubelet_result[spaxel_key]
        
        xdata = spaxel_result_dict['xdata']
        ydata = spaxel_result_dict['ydata']
        noise = spaxel_result_dict['noise']
        
        if i == 0:
            max_num_components = spaxel_result_dict['max_num_components']
        else:
            assert (spaxel_result_dict['max_num_components'] ==
                    max_num_components)
        
        num_components = 0
        
        prev_mean_residual = _np.nanmean(_np.abs(ydata))
        
        for j in range(1, max_num_components + 1):
            
            mean_residual = _np.nanmean(_np.abs(
                spaxel_result_dict[j]['residual_data']))
            
            if prev_mean_residual < mean_residual:
                break
            else:
                prev_mean_residual = mean_residual
            
            fit_error = spaxel_result_dict[j]['fit_error']
            aon = (spaxel_result_dict[j]['peak_to_fit'] /
                   spaxel_result_dict['noise'])
            
            amplitude_index = fit_model.get_amplitude_index_of_component(j)
            
            fit_amplitude = (
                spaxel_result_dict[j]['fit_params'][amplitude_index])
            
            aon_fit = fit_amplitude / spaxel_result_dict['noise']
            
            if ((fit_error == 0) and (aon > best_fit_options['aon']) and
                (aon_fit > best_fit_options['aon'])):
                if 'min_a' in best_fit_options.keys():
                    if fit_amplitude > best_fit_options['min_a']:
                        num_components = j
                    else:
                        break
                else:
                    num_components = j
            else:
                break
        
        if num_components != 0:
            
            fit_error = spaxel_result_dict[num_components]['fit_error']
            peak_to_fit = spaxel_result_dict[num_components]['peak_to_fit']
            fit_params = spaxel_result_dict[num_components]['fit_params']
            fit_ydata = spaxel_result_dict[num_components]['fit_ydata']
            residual_data = spaxel_result_dict[num_components]['residual_data']
            goodness_of_fit = spaxel_result_dict[num_components]['goodness_of_fit']
            
        else:
            
            fit_error = 0
            peak_to_fit = spaxel_result_dict[1]['peak_to_fit']
            fit_params = _np.array([])
            fit_ydata = _np.nan * ydata
            residual_data = ydata
            goodness_of_fit = _np.nan


        if ('sort_components' in best_fit_options.keys()):
            fit_params = _sort_components_of_best_fit(
                fit_params, num_components, fit_model, best_fit_options,
                spaxel_key=spaxel_key, line_rf_wl=line_rf_wl,
                rf_resolution=rf_resolution)
        
        extended_fit_params = (_np.nan *
            spaxel_result_dict[max_num_components]['fit_params'])
        extended_fit_params[:len(fit_params)] = fit_params[:]

        best_fit_result[spaxel_key]['xdata'] = xdata
        best_fit_result[spaxel_key]['ydata'] = ydata
        best_fit_result[spaxel_key]['noise'] = noise
        best_fit_result[spaxel_key]['max_num_components'] = max_num_components
        
        best_fit_result[spaxel_key]['num_components'] = num_components
        best_fit_result[spaxel_key]['fit_error'] = fit_error
        best_fit_result[spaxel_key]['peak_to_fit'] = peak_to_fit
        best_fit_result[spaxel_key]['fit_params'] = fit_params
        best_fit_result[spaxel_key]['extended_fit_params'] = extended_fit_params
        best_fit_result[spaxel_key]['fit_ydata'] = fit_ydata
        best_fit_result[spaxel_key]['residual_data'] = residual_data
        best_fit_result[spaxel_key]['goodness_of_fit'] = goodness_of_fit
    
    return best_fit_result


def _get_array_from_key_in_pseudo_array(best_fit_result, key):
    
    value = best_fit_result[0, 0][key]
    
    if type(value) is _np.ndarray:
        dim_array = 3
        num_pix_wl = len(value)
    else:
        dim_array = 2

    max_index_y, max_index_x = max(best_fit_result.keys())
    
    num_pix_y, num_pix_x = max_index_y + 1, max_index_x + 1
    
    if dim_array == 3:
        
        result = _np.zeros((num_pix_wl, num_pix_y, num_pix_x)) + _np.nan
        
        for i in range(num_pix_x):
            for j in range(num_pix_y):
                result[:, j, i] = best_fit_result[j, i][key]
    
    else:
        
        result = _np.zeros((num_pix_y, num_pix_x)) + _np.nan
        
        for i in range(num_pix_x):
            for j in range(num_pix_y):
                result[j, i] = best_fit_result[j, i][key]
    
    return result


def _save_best_fit(best_fit_filename, hdr, best_fit_result,
                   fit_model, line_rf_wl, rf_resolution):

    # Get the data for the HDUs
    
    ydata = _get_array_from_key_in_pseudo_array(
        best_fit_result, 'ydata')
    
    fit_ydata = _get_array_from_key_in_pseudo_array(
        best_fit_result, 'fit_ydata')

    extended_fit_params = _get_array_from_key_in_pseudo_array(
        best_fit_result, 'extended_fit_params')

    num_components = _get_array_from_key_in_pseudo_array(
        best_fit_result, 'num_components')
    
    peak_to_fit = _get_array_from_key_in_pseudo_array(
        best_fit_result, 'peak_to_fit')
    
    noise = _get_array_from_key_in_pseudo_array(
        best_fit_result, 'noise')
    
    goodness_of_fit = _get_array_from_key_in_pseudo_array(
        best_fit_result, 'goodness_of_fit')
    
    # Get the scientific parameters
    
    sci_params = fit_model.fit_to_sci_params_cube(
        extended_fit_params, line_rf_wl, rf_resolution)

    # Create the HDUs for the FITS file
    
    cropped_three_d_hdr = hdr.copy()
    _remove_kwds_of_third_axis(cropped_three_d_hdr)

    two_d_hdr = hdr.copy()
    _remove_kwds_of_third_axis(two_d_hdr)

    primary_hdu = _fits.PrimaryHDU()
    
    spec_hdu = _fits.ImageHDU(name='SPEC',
        data=ydata, header=hdr)
    
    fit_spec_hdu = _fits.ImageHDU(name='FIT_SPEC',
        data=fit_ydata, header=hdr)
    
    fit_params_hdu = _fits.ImageHDU(name='FIT_PARAMS',
        data=extended_fit_params, header=cropped_three_d_hdr)
    
    sci_params_hdu = _fits.ImageHDU(name='SCI_PARAMS',
        data=sci_params, header=cropped_three_d_hdr)
    
    num_comp_hdu = _fits.ImageHDU(name='NUM_COMP',
        data=num_components, header=two_d_hdr)
    
    peak_hdu = _fits.ImageHDU(name='PEAK_TO_FIT',
        data=peak_to_fit, header=two_d_hdr)
    
    noise_hdu = _fits.ImageHDU(name='NOISE',
        data=noise, header=two_d_hdr)
    
    chi_hdu = _fits.ImageHDU(name='CHI',
        data=goodness_of_fit, header=two_d_hdr)
    
    # Create the FITS file

    list_of_hdu = [
        primary_hdu, spec_hdu, fit_spec_hdu, fit_params_hdu, sci_params_hdu,
        num_comp_hdu, peak_hdu, noise_hdu, chi_hdu]

    hdu_list = _fits.HDUList(list_of_hdu)
    hdu_list.writeto(best_fit_filename, overwrite=True)


def get_fit(control_file):
    """
    Main function of ``fit`` file
    """
    
    _logging.info('starting fit step for control file {}...'.format(
                   control_file))
    
    # Read the YAML control file
    
    control_dict = _read_control_file(control_file)
    
    # De-reference the information from the control file to be used in this tool
    
    output_prefix = control_dict['output_prefix']
    redshift = control_dict['redshift']
    all_cubelets_dict = control_dict['cubelets']
    
    # Fit each cubelet as requested
    
    for cubelet_name, cubelet_dict in all_cubelets_dict.items():
        
        cubelet_filename = '{}-{}.fits'.format(output_prefix, cubelet_name)
        
        with _fits.open(cubelet_filename, mode='readonly') as hdu_list:
            hdr = hdu_list[0].header.copy()
        
        hdr['REDSHIFT'] = redshift
        
        line_rf_wl = cubelet_dict['line_rf_wl']
        rf_resolution = cubelet_dict['rf_resolution']
        cont_rf_window_list = cubelet_dict['cont_rf_window_list']
        
        if 'fittings' in cubelet_dict:
            
            for fitting_name, fitting_dict in cubelet_dict['fittings'].items():
                
                fit_model_name = fitting_dict['fit_model']
                fit_model_params = fitting_dict['fit_model_params']
                
                fittings_filename = '{}-{}-fitting-{}.hdf5'.format(
                    output_prefix, cubelet_name, fitting_name)
                best_fit_filename = '{}-{}-fitting-{}.fits'.format(
                    output_prefix, cubelet_name, fitting_name)
                
                _logging.info('fitting cubelet {} into {}/{}...'.format(
                    cubelet_filename, fittings_filename, best_fit_filename))
                
                
                fit_model = _get_model_from_name(fit_model_name,
                                                 fit_model_params)
                
                multi_fit_cubelet_result = _fit_one_cubelet(
                    cubelet_filename, redshift, rf_resolution,
                    max_num_components=fitting_dict['max_num_components'],
                    fit_model=fit_model,
                    fit_options=fitting_dict['fit_options'],
                    cont_rf_window_list=cont_rf_window_list)
                
                root_attrs = {'redshift': redshift, 'fitmodel': fit_model_name}
                _write_multifitting_to_hdf5(
                    fittings_filename, multi_fit_cubelet_result,
                    root_attrs=root_attrs)
                
                best_fit_result = _get_best_fit_based_on_aon(
                    multi_fit_cubelet_result, fitting_dict['best_fit_options'],
                    fit_model=fit_model, line_rf_wl=line_rf_wl,
                    rf_resolution=rf_resolution)
                
                hdr['FITMODEL'] = fit_model_name
                
                _save_best_fit(best_fit_filename, hdr, best_fit_result,
                               fit_model, line_rf_wl, rf_resolution)


def _main(opts=None):
    
    # Parse command line switches
    
    if opts is None:
        args = _sys.argv[1:]
        
        parser = get_parser()
        opts = parser.parse_args(args)
    
    # Start off the logging and silent warnings (if requested)
    
    _set_logging_config(opts.log_level)
    _set_warning_config(opts.lib_warnings)
    
    # Call the main function
    
    get_fit(opts.control_file)


if __name__ == '__main__':
    _sys.exit(_main())

