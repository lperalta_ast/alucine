#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ALUCINE: A set of tools for emission line fitting of astronomical IFU data
# Copyright (C) 2022  Luis Peralta de Arriba
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
``full`` file
"""


import logging as _logging
import sys as _sys
from argparse import ArgumentParser as _ArgumentParser

from .utils.logging import add_logging_to_parser as _add_logging_to_parser
from .utils.logging import set_logging_config    as _set_logging_config
from .utils.logging import set_warning_config    as _set_warning_config
from .preview       import get_preview           as _get_preview
from .cubelets      import get_cubelets          as _get_cubelets
from .moments       import get_moments           as _get_moments
from .fit           import get_fit               as _get_fit
from .plot          import get_plot              as _get_plot


def get_parser(add_help=True):
    """
    Parser of ``full`` file
    """
    
    parser = _ArgumentParser(add_help=add_help,
        description='full ALUCINE: all steps at once')
    
    parser.add_argument('control_file',
                        help='a YAML file with the control instructions')
    
    _add_logging_to_parser(parser)
    
    parser.set_defaults(func=_main)
    
    return parser


def full_process(control_file):
    """
    Main function of ``full`` file
    """
    
    _logging.info('starting to run all steps at once on file {}...'.format(
                   control_file))
    
    # Call each ALUCINE step individually
    
    _get_preview(control_file, interactive=False)
    _get_cubelets(control_file)
    _get_moments(control_file)
    _get_fit(control_file)
    _get_plot(control_file, interactive=False)


def _main(opts=None):
    
    # Parse command line switches
    
    if opts is None:
        args = _sys.argv[1:]
    
        parser = get_parser()
        opts = parser.parse_args(args)
    
    # Start off the logging and silent warnings (if requested)
    
    _set_logging_config(opts.log_level)
    _set_warning_config(opts.lib_warnings)
    
    # Call the main function
    
    full_process(opts.control_file)


if __name__ == '__main__':
    _sys.exit(_main())

