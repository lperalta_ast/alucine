#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ALUCINE: A set of tools for emission line fitting of astronomical IFU data
# Copyright (C) 2022  Luis Peralta de Arriba
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
``main`` file
"""


import sys as _sys
from argparse import ArgumentParser as _ArgumentParser

from alucine import __version__ as _alucine_version
from alucine.full import get_parser as _full_get_parser

from alucine.preview  import get_parser as _preview_get_parser
from alucine.cubelets import get_parser as _cubelets_get_parser
from alucine.moments  import get_parser as _moments_get_parser
from alucine.fit      import get_parser as _fit_get_parser
from alucine.plot     import get_parser as _plot_get_parser

from alucine.tools.ap_extract   import get_parser as _ap_extract_get_parser
from alucine.tools.filter       import get_parser as _filter_get_parser
from alucine.tools.flux_hz_to_a import get_parser as _flux_hz_to_a_get_parser
from alucine.tools.prepare      import get_parser as _prepare_get_parser
from alucine.tools.slice_hist   import get_parser as _slice_hist_get_parser


def get_parser():
    """
    Parser of ``main`` file
    """
    
    parser = _ArgumentParser(description=
    """
    ALUCINE, a Python package designed for line fitting of astronomical data
    observed with integral field units.
    """)
    
    parser.add_argument('--version', action='version',
                        version='%(prog)s version {}'.format(_alucine_version))
    
    parser.set_defaults(func=lambda opts: parser.print_help())
    
    subparsers = parser.add_subparsers()
    
    # Create the parsers for each step command
    
    parent_parser = _preview_get_parser(add_help=False)
    parser_preview = subparsers.add_parser('preview',
        parents=[parent_parser], description=parent_parser.description,
        help=parent_parser.description)
    
    parent_parser = _cubelets_get_parser(add_help=False)
    parser_cubelets = subparsers.add_parser('cubelets',
        parents=[parent_parser], description=parent_parser.description,
        help=parent_parser.description)
    
    parent_parser = _moments_get_parser(add_help=False)
    parser_moments = subparsers.add_parser('moments',
        parents=[parent_parser], description=parent_parser.description,
        help=parent_parser.description)
    
    parent_parser = _fit_get_parser(add_help=False)
    parser_fit = subparsers.add_parser('fit',
        parents=[parent_parser], description=parent_parser.description,
        help=parent_parser.description)
    
    parent_parser = _plot_get_parser(add_help=False)
    parser_plot = subparsers.add_parser('plot',
        parents=[parent_parser], description=parent_parser.description,
        help=parent_parser.description)
    
    # Create the parser for the full command
    
    parent_parser = _full_get_parser(add_help=False)
    parser_full = subparsers.add_parser('full',
        parents=[parent_parser], description=parent_parser.description,
        help=parent_parser.description)
    
    # Create the parser for the tools commands
    
    parser_tools = subparsers.add_parser('tools', description='ALUCINE tools',
        help='ALUCINE tools (see "alucine tools -h" for more information)')
    
    parser_tools.set_defaults(func=lambda opts: parser_tools.print_help())
    
    subparsers_tools = parser_tools.add_subparsers()
    
    # Create the parsers for each tools command
    
    parent_parser = _ap_extract_get_parser(add_help=False)
    parser_ap_extract = subparsers_tools.add_parser('ap-extract',
        parents=[parent_parser], description=parent_parser.description,
        help=parent_parser.description)
    
    parent_parser = _filter_get_parser(add_help=False)
    parser_filter = subparsers_tools.add_parser('filter',
        parents=[parent_parser], description=parent_parser.description,
        help=parent_parser.description)
    
    parent_parser = _flux_hz_to_a_get_parser(add_help=False)
    parser_flux_hz_to_a = subparsers_tools.add_parser('flux-hz-to-a',
        parents=[parent_parser], description=parent_parser.description,
        help=parent_parser.description)
    
    parent_parser = _prepare_get_parser(add_help=False)
    parser_prepare = subparsers_tools.add_parser('prepare',
        parents=[parent_parser], description=parent_parser.description,
        help=parent_parser.description)
    
    parent_parser = _slice_hist_get_parser(add_help=False)
    parser_slice_hist = subparsers_tools.add_parser('slice-hist',
        parents=[parent_parser], description=parent_parser.description,
        help=parent_parser.description)
    
    return parser


def main(opts=None):
    """
    Main function of ``main`` file
    """
    
    # Parse command line switches
    
    if opts is None:
        args = _sys.argv[1:]
    
        parser = get_parser()
        opts = parser.parse_args(args)
    
    # Call the corresponding ALUCINE subcommand
    
    opts.func(opts)


if __name__ == '__main__':
    _sys.exit(main())

