#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ALUCINE: A set of tools for emission line fitting of astronomical IFU data
# Copyright (C) 2022  Luis Peralta de Arriba
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
``sii_doublet`` file
"""


import numpy as _np

from .base import FitModel as _FitModel


class SiiDoubletGaussianWithBaseline(_FitModel):
    """
    ``SiiDoubletGaussianWithBaseline``
    """
    
    def __init__(self, model_params=None):
        
        self.__name__ = 'SiiDoubletGaussianWithBaseline'
        
        self.baseline_num_params = 1
        self.component_num_params = 4
        
        self.component_type_fit_params = ('A', 'c', 'w', 'A')
        self.component_type_sci_params = ('I', 'v', 'd', 'I')
        self.component_name_fit_params = ('$A_1$', '$c_1$', '$w$', '$A_2$')
        self.component_name_sci_params = ('$I_1$', '$v$', '$\sigma$', '$I_2$')
        
        super()._init_check()
    
    def fit_function_for_baseline(self, xdata, *params):

        baseline = _np.zeros(len(xdata)) + params[0]
        
        return baseline
    
    def fit_function_for_component(self, xdata, *params):
    
        main_line = (params[0] * _np.exp((-(xdata - params[1])**2) /
                                        (2.0 * params[2]**2)))
                                        
        redshift = (params[1] / 6716.31 - 1.0)
    
        aux_line = (params[3] *
                    _np.exp((-(xdata - (1.0 + redshift) * 6730.68)**2) /
                    (2.0 * params[2]**2)))
        
        component = main_line + aux_line
        
        return component
    
    def get_baseline_low_bounds(self, xdata, ydata, noise=None,
                                cont_rf_window_list=None):
        
        baseline_low_bounds = _np.array([_np.nanmin(ydata)])
        
        return baseline_low_bounds
    
    def get_baseline_high_bounds(self, xdata, ydata, noise=None,
                                 cont_rf_window_list=None):
        
        baseline_high_bounds = _np.array([_np.nanmax(ydata)])
        
        return baseline_high_bounds
    
    def get_component_low_bounds(self, xdata, ydata, noise=None,
                                 rf_resolution=None):
        
        component_low_bounds = _np.array(
            [0.0, xdata[0], 2.0 * (xdata[-1] - xdata[0]) / len(xdata), 0.0])
        
        return component_low_bounds
    
    def get_component_high_bounds(self, xdata, ydata, noise=None,
                                  rf_resolution=None):

        y_max = _np.nanmax(ydata)

        component_high_bounds = _np.array(
            [2 * y_max, xdata[-1], (xdata[-1] - xdata[0]) / 5.0, 2 * y_max])
        
        return component_high_bounds
    
    def get_baseline_init_params(self, xdata, ydata, low_bounds, high_bounds):
        
        baseline_param0 = 0.0
        
        if ((baseline_param0 < low_bounds[0]) or
            (baseline_param0 > high_bounds[0])):
            baseline_param0 = (high_bounds[0] + low_bounds[0]) / 2.0
        
        baseline_init_params = _np.array([baseline_param0])
        
        return baseline_init_params
    
    def get_component_init_params(self, xdata, ydata, low_bounds, high_bounds):
        
        left_peak_x, left_peak_y, right_peak_x, right_peak_y = (
            self._get_two_peaks(xdata, ydata))

        width = (
            (2 * _np.pi)**-0.5 * _np.nansum(ydata) /
            (left_peak_y + right_peak_y) *
            (xdata[-1] - xdata[0]) / len(xdata))
        
        component_param0 = left_peak_y
        component_param1 = left_peak_x
        component_param2 = width
        component_param3 = right_peak_y
        
        # max_index = _np.nanargmax(ydata, axis=None)
        # y_max = ydata[max_index]
        
        # component_param0 = y_max
        # component_param1 = xdata[max_index]
        # component_param2 = (1.0 / 1.350 *
        #     (2 * _np.pi)**-0.5 * _np.nansum(ydata) / component_param0 *
        #     (xdata[-1] - xdata[0]) / len(xdata))
        # component_param3 = y_max
        
        component_param_list = [
            component_param0, component_param1, component_param2,
            component_param3]
        
        for i in range(len(component_param_list)):
            if ((component_param_list[i] < low_bounds[i]) or
                (component_param_list[i] > high_bounds[i])):
                component_param_list[i] = (high_bounds[i] + low_bounds[i]) / 2.0
        
        component_init_params = _np.array(component_param_list)
        
        return component_init_params

    def fit_to_sci_params_cube(self, fit_params_cube,
                               line_rf_wl, rf_resolution):

        num_params, _, _ = fit_params_cube.shape

        sci_params_cube = _np.zeros(fit_params_cube.shape)

        for index in range(num_params):

            if index < self.baseline_num_params:
                sci_params_cube[index, :, :] = fit_params_cube[index, :, :]
            else:

                comp_param_index = ((index - self.baseline_num_params) %
                                    self.component_num_params)

                if comp_param_index == 0:
                    sci_params_cube[index, :, :] = (
                        self._amplitude_and_width_to_intensity(
                            fit_params_cube[index, :, :],
                            fit_params_cube[index + 2, :, :]))
                elif comp_param_index == 1:
                    sci_params_cube[index, :, :] = (
                        self._center_to_velocity(
                            fit_params_cube[index, :, :], line_rf_wl))
                elif comp_param_index == 2:
                    sci_params_cube[index, :, :] = (
                        self._width_to_dispersion(
                            fit_params_cube[index, :, :], line_rf_wl,
                            rf_resolution))
                elif comp_param_index == 3:
                    sci_params_cube[index, :, :] = (
                        self._amplitude_and_width_to_intensity(
                            fit_params_cube[index, :, :],
                            fit_params_cube[index - 1, :, :]))
                else:
                    raise ValueError

        return sci_params_cube


class SiiDoubletGaussianWithStraightLine(SiiDoubletGaussianWithBaseline):
    """
    ``SiiDoubletGaussianWithStraightLine``
    """
    
    def __init__(self, model_params=None):
        
        self.__name__ = 'SiiDoubletGaussianWithStraightLine'
        
        self.baseline_num_params = 2
        self.component_num_params = 4
        
        self.component_type_fit_params = ('A', 'c', 'w', 'A')
        self.component_type_sci_params = ('I', 'v', 'd', 'I')
        self.component_name_fit_params = ('$A_1$', '$c_1$', '$w$', '$A_2$')
        self.component_name_sci_params = ('$I_1$', '$v$', '$\sigma$', '$I_2$')
        
        super()._init_check()
    
    def fit_function_for_baseline(self, xdata, *params):

        baseline = (_np.zeros(len(xdata)) +
                    params[0] + (params[1] - params[0]) *
                    (xdata - xdata[0]) / (xdata[-1] - xdata[0]))
        
        return baseline
    
    def _get_baseline_levels_at_continuum(self, xdata, ydata,
                                          cont_rf_window_list=None):
        
        assert len(cont_rf_window_list) == 2
        assert (cont_rf_window_list[0][0] < cont_rf_window_list[0][1])
        assert (cont_rf_window_list[1][0] < cont_rf_window_list[1][1])
        assert (cont_rf_window_list[0][1] < cont_rf_window_list[1][0])
        
        cont_channels0 = ((xdata > cont_rf_window_list[0][0]) *
                          (xdata < cont_rf_window_list[0][1]))
        
        baseline_level0 = _np.median(ydata[cont_channels0])
        
        cont_channels1 = ((xdata > cont_rf_window_list[1][0]) *
                          (xdata < cont_rf_window_list[1][1]))
        
        baseline_level1 = _np.median(ydata[cont_channels1])
        
        return baseline_level0, baseline_level1
    
    def get_baseline_low_bounds(self, xdata, ydata, noise=None,
                                cont_rf_window_list=None):
        
        baseline_level0, baseline_level1 = (
            self._get_baseline_levels_at_continuum(
                xdata, ydata, cont_rf_window_list=cont_rf_window_list))
        
        if noise is not None:
            baseline_low_bounds = _np.array([baseline_level0 - 3.0 * noise,
                                             baseline_level1 - 3.0 * noise])
        else:
            raise NotImplementedError
            #baseline_low_bounds = _np.array([_np.nanmin(ydata)])
        
        return baseline_low_bounds
    
    def get_baseline_high_bounds(self, xdata, ydata, noise=None,
                                 cont_rf_window_list=None):
        
        baseline_level0, baseline_level1 = (
            self._get_baseline_levels_at_continuum(
                xdata, ydata, cont_rf_window_list=cont_rf_window_list))
        
        if noise is not None:
            baseline_high_bounds = _np.array([baseline_level0 + 3.0 * noise,
                                              baseline_level1 + 3.0 * noise])
        else:
            raise NotImplementedError
            #baseline_high_bounds = _np.array([_np.nanmax(ydata)])
        
        return baseline_high_bounds
    
    def get_baseline_init_params(self, xdata, ydata, low_bounds, high_bounds):
        
        baseline_param0 = (high_bounds[0] + low_bounds[0]) / 2.0
        
        baseline_param1 = (high_bounds[1] + low_bounds[1]) / 2.0
        
        baseline_init_params = _np.array([baseline_param0, baseline_param1])
        
        return baseline_init_params

