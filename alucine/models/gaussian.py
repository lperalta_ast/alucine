#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ALUCINE: A set of tools for emission line fitting of astronomical IFU data
# Copyright (C) 2022  Luis Peralta de Arriba
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
``gaussian`` file
"""


import numpy as _np

from .base import FitModel as _FitModel


class GaussianWithBaseline(_FitModel):
    """
    ``GaussianWithBaseline``
    """
    
    def __init__(self, model_params=None):
        
        self.__name__ = 'GaussianWithBaseline'
        
        self.baseline_num_params = 1
        self.component_num_params = 3
        
        self.component_type_fit_params = ('A', 'c', 'w')
        self.component_type_sci_params = ('I', 'v', 'd')
        self.component_name_fit_params = ('$A$', '$c$', '$w$')
        self.component_name_sci_params = ('$I$', '$v$', '$\sigma$')
        
        super()._init_check()
    
    def fit_function_for_baseline(self, xdata, *params):

        baseline = _np.zeros(len(xdata)) + params[0]
        
        return baseline
    
    def fit_function_for_component(self, xdata, *params):
    
        component = (params[0] * _np.exp((-(xdata - params[1])**2) /
                                        (2.0 * params[2]**2)))
        
        return component
    
    def get_baseline_low_bounds(self, xdata, ydata, noise=None,
                                cont_rf_window_list=None):
        
        if noise is not None:
            baseline_low_bounds = _np.array([-noise])
        else:
            raise NotImplementedError
            #baseline_low_bounds = _np.array([_np.nanmin(ydata)])
        
        return baseline_low_bounds
    
    def get_baseline_high_bounds(self, xdata, ydata, noise=None,
                                cont_rf_window_list=None):
        
        if noise is not None:
            baseline_high_bounds = _np.array([noise])
        else:
            raise NotImplementedError
            #baseline_high_bounds = _np.array([_np.nanmax(ydata)])
        
        return baseline_high_bounds
    
    def get_component_low_bounds(self, xdata, ydata, noise=None,
                                 rf_resolution=None):
        
        if (noise is not None) and (rf_resolution is not None):
            low_amplitude = _np.nanmin([0.5 * noise, 2.0 * _np.nanmax(ydata)])
            low_width = rf_resolution / (2 * _np.sqrt(2 * _np.log(2)))
            component_low_bounds = _np.array(
                [low_amplitude, xdata[0], low_width])
        else:
            raise NotImplementedError
        
#        component_low_bounds = _np.array(
#            [0.1 * _np.nanmax(ydata), xdata[0], 0.5 * (xdata[-1] - xdata[0]) / len(xdata)])
        
        return component_low_bounds
    
    def get_component_high_bounds(self, xdata, ydata, noise=None,
                                  rf_resolution=None):
        
        if (noise is not None):
            high_amplitude = _np.nanmax([0.5 * noise, 2.0 * _np.nanmax(ydata)])
            component_high_bounds = _np.array(
                [high_amplitude, xdata[-1], (xdata[-1] - xdata[0]) / 5.0])
        else:
            raise NotImplementedError
        
#        component_high_bounds = _np.array(
#            [1.5 * _np.nanmax(ydata), xdata[-1], (xdata[-1] - xdata[0]) / 2.0])
        
        return component_high_bounds
    
    def get_baseline_init_params(self, xdata, ydata, low_bounds, high_bounds):
        
        baseline_param0 = 0.0
        
        if ((baseline_param0 < low_bounds[0]) or
            (baseline_param0 > high_bounds[0])):
            baseline_param0 = (high_bounds[0] + low_bounds[0]) / 2.0
        
        baseline_init_params = _np.array([baseline_param0])
        
        return baseline_init_params
    
    def get_component_init_params(self, xdata, ydata, low_bounds, high_bounds):
        
        max_index = _np.nanargmax(ydata, axis=None)
        max_x = xdata[max_index]
        max_y = ydata[max_index]

        width = (
            (2 * _np.pi)**-0.5 * _np.nansum(ydata) / max_y *
            (xdata[-1] - xdata[0]) / len(xdata))
        
        component_param0 = max_y
        component_param1 = max_x
        component_param2 = width
        
        component_param_list = [
            component_param0, component_param1, component_param2]
        
        for i in range(len(component_param_list)):
            if ((component_param_list[i] < low_bounds[i]) or
                (component_param_list[i] > high_bounds[i])):
                component_param_list[i] = (high_bounds[i] + low_bounds[i]) / 2.0
        
        component_init_params = _np.array(component_param_list)
        
        return component_init_params

    def fit_to_sci_params_cube(self, fit_params_cube,
                               line_rf_wl, rf_resolution):

        num_params, _, _ = fit_params_cube.shape

        sci_params_cube = _np.zeros(fit_params_cube.shape)

        for index in range(num_params):

            if index < self.baseline_num_params:
                sci_params_cube[index, :, :] = fit_params_cube[index, :, :]
            else:

                comp_param_index = ((index - self.baseline_num_params) %
                                    self.component_num_params)

                if comp_param_index == 0:
                    sci_params_cube[index, :, :] = (
                        self._amplitude_and_width_to_intensity(
                            fit_params_cube[index, :, :],
                            fit_params_cube[index + 2, :, :]))
                elif comp_param_index == 1:
                    sci_params_cube[index, :, :] = (
                        self._center_to_velocity(
                            fit_params_cube[index, :, :], line_rf_wl))
                elif comp_param_index == 2:
                    sci_params_cube[index, :, :] = (
                        self._width_to_dispersion(
                            fit_params_cube[index, :, :], line_rf_wl,
                            rf_resolution))
                else:
                    raise ValueError

        return sci_params_cube


class GaussianWithStraightLine(GaussianWithBaseline):
    """
    ``GaussianWithStraightLine``
    """
    
    def __init__(self, model_params=None):
        
        self.__name__ = 'GaussianWithStraightLine'
        
        self.baseline_num_params = 2
        self.component_num_params = 3
        
        self.component_type_fit_params = ('A', 'c', 'w')
        self.component_type_sci_params = ('I', 'v', 'd')
        self.component_name_fit_params = ('$A$', '$c$', '$w$')
        self.component_name_sci_params = ('$I$', '$v$', '$\sigma$')
        
        super()._init_check()
    
    def fit_function_for_baseline(self, xdata, *params):

        baseline = (_np.zeros(len(xdata)) +
                    params[0] + (params[1] - params[0]) *
                    (xdata - xdata[0]) / (xdata[-1] - xdata[0]))
        
        return baseline
    
    def _get_baseline_levels_at_continuum(self, xdata, ydata,
                                          cont_rf_window_list=None):
        
        assert len(cont_rf_window_list) == 2
        assert (cont_rf_window_list[0][0] < cont_rf_window_list[0][1])
        assert (cont_rf_window_list[1][0] < cont_rf_window_list[1][1])
        assert (cont_rf_window_list[0][1] < cont_rf_window_list[1][0])
        
        cont_channels0 = ((xdata > cont_rf_window_list[0][0]) *
                          (xdata < cont_rf_window_list[0][1]))
        
        baseline_level0 = _np.median(ydata[cont_channels0])
        
        cont_channels1 = ((xdata > cont_rf_window_list[1][0]) *
                          (xdata < cont_rf_window_list[1][1]))
        
        baseline_level1 = _np.median(ydata[cont_channels1])
        
        return baseline_level0, baseline_level1
    
    def get_baseline_low_bounds(self, xdata, ydata, noise=None,
                                cont_rf_window_list=None):
        
        baseline_level0, baseline_level1 = (
            self._get_baseline_levels_at_continuum(
                xdata, ydata, cont_rf_window_list=cont_rf_window_list))
        
        if noise is not None:
            baseline_low_bounds = _np.array([baseline_level0 - 3.0 * noise,
                                             baseline_level1 - 3.0 * noise])
        else:
            raise NotImplementedError
            #baseline_low_bounds = _np.array([_np.nanmin(ydata)])
        
        return baseline_low_bounds
    
    def get_baseline_high_bounds(self, xdata, ydata, noise=None,
                                 cont_rf_window_list=None):
        
        baseline_level0, baseline_level1 = (
            self._get_baseline_levels_at_continuum(
                xdata, ydata, cont_rf_window_list=cont_rf_window_list))
        
        if noise is not None:
            baseline_high_bounds = _np.array([baseline_level0 + 3.0 * noise,
                                              baseline_level1 + 3.0 * noise])
        else:
            raise NotImplementedError
            #baseline_high_bounds = _np.array([_np.nanmax(ydata)])
        
        return baseline_high_bounds
    
    def get_baseline_init_params(self, xdata, ydata, low_bounds, high_bounds):
        
        baseline_param0 = (high_bounds[0] + low_bounds[0]) / 2.0
        
        baseline_param1 = (high_bounds[1] + low_bounds[1]) / 2.0
        
        baseline_init_params = _np.array([baseline_param0, baseline_param1])
        
        return baseline_init_params

