#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ALUCINE: A set of tools for emission line fitting of astronomical IFU data
# Copyright (C) 2022  Luis Peralta de Arriba
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
``base`` file: it contains the base class for creating fit models
"""


import numpy as _np


class FitModel:
    """
    Base class for creating fit models.
    
    TBW
    `fit_function_for_baseline`, `fit_function_for_component`,
    `get_baseline_low_bounds`, `get_baseline_high_bounds`,
    `get_component_low_bounds`, `get_component_high_bounds`,
    `get_baseline_init_params`, `get_component_init_params`,
    `fit_to_sci_params_cube`
    """
    
    def __init__(self, model_params=None):
        
        self.__name__ = 'FitModel'
        
        self.baseline_num_params = None
        self.component_num_params = None
        
        self.component_type_fit_params = None
        self.component_type_sci_params = None
        self.component_name_fit_params = None
        self.component_name_sci_params = None
    
    def _init_check(self):
        
        assert len(self.component_type_fit_params) == self.component_num_params
        assert len(self.component_type_sci_params) == self.component_num_params
        assert len(self.component_name_fit_params) == self.component_num_params
        assert len(self.component_name_sci_params) == self.component_num_params
    
    def get_baseline_num_params(self):
        return self.baseline_num_params
    
    def get_component_num_params(self):
        return self.component_num_params
    
    def get_component_type_fit_params(self):
        return self.component_type_fit_params
    
    def get_component_type_sci_params(self):
        return self.component_type_sci_params
    
    def get_component_name_fit_params(self):
        return self.component_name_fit_params
    
    def get_component_name_sci_params(self):
        return self.component_name_sci_params
    
    def get_num_params(self, num_components):
        
        num_params = (self.baseline_num_params +
                      num_components * self.component_num_params)
        
        return num_params
    
    def fit_function(self, xdata, *params):
        
        baseline_params = params[:self.baseline_num_params]
        
        result = self.fit_function_for_baseline(xdata, *baseline_params)
        
        all_component_params = params[self.baseline_num_params:]
        
        for i in range(len(all_component_params) // self.component_num_params):
            min_index = (self.baseline_num_params +
                         i * self.component_num_params)
            max_index = (self.baseline_num_params +
                         (i + 1) * self.component_num_params)
            
            component_params = params[min_index:max_index]
            
            result += self.fit_function_for_component(xdata, *component_params)
        
        return result
    
    def fit_function_for_baseline(self, xdata, *params):
        raise NotImplementedError
    
    def fit_function_for_component(self, xdata, *params):
        raise NotImplementedError
    
    def get_baseline_low_bounds(self, xdata, ydata, noise=None,
                                cont_rf_window_list=None):
        raise NotImplementedError
    
    def get_baseline_high_bounds(self, xdata, ydata, noise=None,
                                 cont_rf_window_list=None):
        raise NotImplementedError
    
    def get_component_low_bounds(self, xdata, ydata, noise=None,
                                 rf_resolution=None):
        raise NotImplementedError
    
    def get_component_high_bounds(self, xdata, ydata, noise=None,
                                  rf_resolution=None):
        raise NotImplementedError
    
    def get_baseline_init_params(self, xdata, ydata, low_bounds, high_bounds):
        raise NotImplementedError
    
    def get_component_init_params(self, xdata, ydata, low_bounds, high_bounds):
        raise NotImplementedError

    def _amplitude_and_width_to_intensity(self, amplitude, width):

        intensity = _np.sqrt(2.0 * _np.pi) * amplitude * width

        return intensity

    def _center_to_velocity(self, center, line_rf_wl):

        c_km_s = 299792.458

        velocity = (center - line_rf_wl) / line_rf_wl * c_km_s

        return velocity

    def _width_to_dispersion(self, width, line_rf_wl, rf_resolution):

        c_km_s = 299792.458

        rf_resolution_sigma = rf_resolution / (2 * _np.sqrt(2 * _np.log(2)))

        intrinsic_sigma_sq = width**2 - rf_resolution_sigma**2

        intrinsic_sigma = _np.sqrt(intrinsic_sigma_sq)

        intrinsic_sigma[intrinsic_sigma_sq < 0.0] = (
            -_np.sqrt(-intrinsic_sigma_sq)[intrinsic_sigma_sq < 0.0])

        dispersion = intrinsic_sigma / line_rf_wl * c_km_s

        return dispersion
    
    def _get_two_peaks(self, xdata, ydata, mask_width=10.0):

        higher_peak_index = _np.nanargmax(ydata, axis=None)
        higher_peak_x = xdata[higher_peak_index]
        higher_peak_y = ydata[higher_peak_index]

        min_ydata = _np.nanmin(ydata)
        aux_ydata = ydata.copy()

        aux_mask = _np.logical_and(
            xdata > higher_peak_x - mask_width,
            xdata < higher_peak_x + mask_width)
        aux_ydata[aux_mask] = min_ydata

        lower_peak_index = _np.nanargmax(aux_ydata, axis=None)
        lower_peak_x = xdata[lower_peak_index]
        lower_peak_y = ydata[lower_peak_index]

        if lower_peak_x < higher_peak_x:
            left_peak_x, left_peak_y = lower_peak_x, lower_peak_y
            right_peak_x, right_peak_y = higher_peak_x, higher_peak_y
        else:
            left_peak_x, left_peak_y = higher_peak_x, higher_peak_y
            right_peak_x, right_peak_y = lower_peak_x, lower_peak_y

        return left_peak_x, left_peak_y, right_peak_x, right_peak_y

    def fit_to_sci_params_cube(self, fit_params_cube,
                               line_rf_wl, rf_resolution):
        raise NotImplementedError
    
    def get_amplitude_index_of_component(self, component_j):
        
        amplitude_index = (self.baseline_num_params +
                           (component_j - 1) * self.component_num_params)
        
        return amplitude_index

