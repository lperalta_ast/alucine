#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ALUCINE: A set of tools for emission line fitting of astronomical IFU data
# Copyright (C) 2022  Luis Peralta de Arriba
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
``_get_model_from_name`` file
"""


from .gaussian     import (GaussianWithBaseline,
                           GaussianWithStraightLine)
from .oiii_doublet import (OiiiDoubletGaussianWithBaseline,
                           OiiiDoubletGaussianWithStraightLine)
from .sii_doublet  import (SiiDoubletGaussianWithBaseline,
                           SiiDoubletGaussianWithStraightLine)
from .halpha_nii   import (HalphaNiiGaussianWithBaseline,
                           HalphaNiiGaussianWithStraightLine)


def get_model_from_name(fit_model_name, fit_model_params=None):

    """
    ``get_model_from_name``
    """
    
    if fit_model_name == 'GaussianWithBaseline':
        fit_model = GaussianWithBaseline(fit_model_params)
    elif fit_model_name == 'GaussianWithStraightLine':
        fit_model = GaussianWithStraightLine(fit_model_params)
    elif fit_model_name == 'OiiiDoubletGaussianWithBaseline':
        fit_model = OiiiDoubletGaussianWithBaseline(fit_model_params)
    elif fit_model_name == 'OiiiDoubletGaussianWithStraightLine':
        fit_model = OiiiDoubletGaussianWithStraightLine(fit_model_params)
    elif fit_model_name == 'SiiDoubletGaussianWithBaseline':
        fit_model = SiiDoubletGaussianWithBaseline(fit_model_params)
    elif fit_model_name == 'SiiDoubletGaussianWithStraightLine':
        fit_model = SiiDoubletGaussianWithStraightLine(fit_model_params)
    elif fit_model_name == 'HalphaNiiGaussianWithBaseline':
        fit_model = HalphaNiiGaussianWithBaseline(fit_model_params)
    elif fit_model_name == 'HalphaNiiGaussianWithStraightLine':
        fit_model = HalphaNiiGaussianWithStraightLine(fit_model_params)
    else:
        raise ValueError
    
    assert fit_model.__name__ == fit_model_name
    
    return fit_model

