#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ALUCINE: A set of tools for emission line fitting of astronomical IFU data
# Copyright (C) 2022  Luis Peralta de Arriba
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
``filter`` file
"""


import logging as _logging
from argparse import ArgumentParser as _ArgumentParser
from os.path import basename as _basename
from os.path import exists as _exists
from os.path import splitext as _splitext

from astropy.io.fits import open as _fits_open
from scipy.ndimage import median_filter as _median_filter
from scipy.ndimage import uniform_filter as _uniform_filter

from ..utils.logging import add_logging_to_parser as _add_logging_to_parser
from ..utils.logging import set_logging_config as _set_logging_config
from ..utils.logging import set_warning_config as _set_warning_config


def get_parser(add_help=True):
    """
    Parser of ``filter`` file
    """

    parser = _ArgumentParser(
        add_help=add_help,
        description='tool for filtering (smoothing) a cube')

    parser.add_argument('filename',
                        help='the name of the input FITS file with a data cube')

    parser.add_argument('--filter', default='uniform', dest='filter_name',
                        choices=['uniform', 'median'],
                        help=('the name of the filter to be employed ' +
                              '(default: %(default)s)'))

    parser.add_argument('--size', type=int, default=3,
                        help=('the size of the filter to be employed ' +
                              '(default: %(default)s)'))

    parser.add_argument('--mode', default='nearest',
        choices=['nearest', 'reflect', 'constant', 'mirror', 'wrap'],
        help=('the mode for extending the array beyond the boundaries ' +
              '(default: %(default)s)'))
    
    _add_logging_to_parser(parser)
    
    parser.set_defaults(func=_main)

    return parser


def get_filtered_file(filename, filter_name='uniform', size=3, mode='nearest'):
    """
    Main function of ``filter`` file
    """
    
    _logging.info('filtering file {} with filter {} of size {}...'.format(
                   filename, filter_name, size))
    
    if filter_name == 'uniform':
        filter_function = _uniform_filter
    elif filter_name == 'median':
        filter_function = _median_filter
    else:
        _logging.error('unknown filter name: {}'.format(filter_name))
        raise ValueError
    
    filter_3d_size = (1, size, size)

    with _fits_open(filename, mode='readonly') as hdu_list:
    
        hdu_list[0].data = filter_function(hdu_list[0].data,
                                           size=filter_3d_size, mode=mode)
        
        if size % 2 == 0:
            _logging.warning(
                'ALUCINE has modified the astrometric information because ' +
                'the size is even ({})'.format(size))
            
            hdu_list[0].header['CRPIX1'] += 0.5
            hdu_list[0].header['CRPIX2'] += 0.5
    
        out_filename = ('_{}_{}'.format(filter_name, size)).join(
                        _splitext(_basename(filename)))
        if not _exists(out_filename):
            hdu_list.writeto(out_filename)
            _logging.info('output file written in {}'.format(out_filename))
        else:
            _logging.error(
                'output file {} already exists: nothing done'.format(
                    out_filename))


def _main(opts=None):

    # Parse command line switches
    
    if opts is None:
        args = _sys.argv[1:]
    
        parser = get_parser()
        opts = parser.parse_args(args)
    
    # Start off the logging and silent warnings (if requested)
    
    _set_logging_config(opts.log_level)
    _set_warning_config(opts.lib_warnings)
    
    # Call the main function
    
    get_filtered_file(opts.filename, filter_name=opts.filter_name,
                      size=opts.size, mode=opts.mode)


if __name__ == '__main__':
    _sys.exit(_main())

