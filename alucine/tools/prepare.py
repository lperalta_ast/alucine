#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ALUCINE: A set of tools for emission line fitting of astronomical IFU data
# Copyright (C) 2022  Luis Peralta de Arriba
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
``prepare`` file
"""


import logging as _logging
from argparse import ArgumentParser as _ArgumentParser
from os.path import basename as _basename
from os.path import exists as _exists
from os.path import splitext as _splitext

import numpy as _np
from astropy.io.fits import open as _fits_open
from astropy.io.fits import PrimaryHDU as _PrimaryHDU
from astropy.io.fits import HDUList as _HDUList

from ..utils.logging import add_logging_to_parser as _add_logging_to_parser
from ..utils.logging import set_logging_config as _set_logging_config
from ..utils.logging import set_warning_config as _set_warning_config
from .flux_hz_to_a import get_flux_a_from_hz_for_hdu as _get_flux_a_from_hz_for_hdu


def get_parser(add_help=True):
    """
    Parser of ``prepare`` file
    """

    parser = _ArgumentParser(
        add_help=add_help,
        description='tool for preparing a FITS file to the ALUCINE input ' +
                        'format')

    parser.add_argument('filename',
                        help='the name of the input FITS file with a data cube')
    
    parser.add_argument('--alt_wcs', default=None,
                        help=('alternative WCS to be considered {B,C,D...} ' +
                              '(default: primary WCS)'))
    
    parser.add_argument('--jwst_mrs', action='store_true',
        help='assume that the input format is a JWST/MIRI/MIR_MRS file ' +
             '(it is useful when the input file has been cropped by someone)')
    
    _add_logging_to_parser(parser)
    
    parser.set_defaults(func=_main)

    return parser


def _prepare_jwst_miri_mrs(hdu_list, alt_wcs=None, assume_jwst_mrs=False):

    if assume_jwst_mrs is False:
        assert hdu_list[0].header['TELESCOP'] == 'JWST'
        assert hdu_list[0].header['INSTRUME'] == 'MIRI'
        assert hdu_list[0].header['EXP_TYPE'] == 'MIR_MRS'
    else:
        _logging.warning(
            'ALUCINE has been forced to assume that it is dealing with a ' +
            'JWST/MIRI/MIR_MRS file')

    hdu = _PrimaryHDU(data=hdu_list['SCI'].data, header=hdu_list['SCI'].header)

    assert hdu.header['BUNIT'] == 'MJy/sr'
    assert hdu.header['CUNIT1'] == 'deg'
    assert hdu.header['CUNIT2'] == 'deg'
    assert hdu.header['CUNIT3'] == 'um'

    assert hdu.header['PC1_1'] == -1.0
    assert hdu.header['PC1_2'] == 0.0
    assert hdu.header['PC1_3'] == 0.0
    assert hdu.header['PC2_1'] == 0.0
    assert hdu.header['PC2_2'] == 1.0
    assert hdu.header['PC2_3'] == 0.0
    assert hdu.header['PC3_1'] == 0.0
    assert hdu.header['PC3_2'] == 0.0
    assert hdu.header['PC3_3'] == 1.0

    hdu.header['CDELT3'] *= 1e4
    hdu.header['CRVAL3'] *= 1e4
    hdu.header['CUNIT3'] = 'Angstrom'

    pix_area_deg2 = hdu.header['CDELT1'] * hdu.header['CDELT2']
    pix_area_sr = (_np.pi / 180)**2 * pix_area_deg2
    hdu.data *= 1e6 * pix_area_sr
    hdu.header['BUNIT'] = 'Jy'

    hdu.header['TELESCOP'] = ('JWST', 'Telescope used to acquire the data')
    hdu.header['INSTRUME'] = ('MIRI', 'Instrument used to acquire the data')
    hdu.header['EXP_TYPE'] = ('MIR_MRS', 'Type of data in the exposure')
    
    new_hdu_list = _HDUList([hdu])

    new_hdu_list[0] = _get_flux_a_from_hz_for_hdu(new_hdu_list[0], alt_wcs=alt_wcs)
    
    return new_hdu_list


def get_prepared_file(filename, alt_wcs=None, assume_jwst_mrs=False):
    """
    Main function of ``prepare`` file
    """
    
    _logging.info('preparing file {} for ALUCINE input format...'.format(
                   filename))
    
    prepared_flag = False

    with _fits_open(filename, mode='readonly') as hdu_list:
    
        if ((('TELESCOP' in hdu_list[0].header) and
            (hdu_list[0].header['TELESCOP'] == 'JWST') and
            ('INSTRUME' in hdu_list[0].header) and
            (hdu_list[0].header['INSTRUME'] == 'MIRI') and
            ('EXP_TYPE' in hdu_list[0].header) and
            (hdu_list[0].header['EXP_TYPE'] == 'MIR_MRS')) or
            (assume_jwst_mrs is True)):
        
            prepared_flag = True
            
            new_hdu_list = _prepare_jwst_miri_mrs(
                hdu_list, alt_wcs=alt_wcs, assume_jwst_mrs=assume_jwst_mrs)
        
        else:
            
            prepared_flag = False
            
            new_hdu_list = None
    
    if prepared_flag is True:
    
        out_filename = '_prepared'.join(_splitext(_basename(filename)))
        if not _exists(out_filename):
            new_hdu_list.writeto(out_filename)
            _logging.info('output file written in {}'.format(out_filename))
        else:
            _logging.error(
                'output file {} already exists: nothing done'.format(
                    out_filename))
    else:
        _logging.warning(
            ('nothing done for file {} because it does not look like a ' +
             '"known" case: it could be prepared or not ').format(filename))


def _main(opts=None):

    # Parse command line switches
    
    if opts is None:
        args = _sys.argv[1:]
    
        parser = get_parser()
        opts = parser.parse_args(args)
    
    # Start off the logging and silent warnings (if requested)
    
    _set_logging_config(opts.log_level)
    _set_warning_config(opts.lib_warnings)
    
    # Call the main function
    
    get_prepared_file(opts.filename, alt_wcs=opts.alt_wcs,
                      assume_jwst_mrs=opts.jwst_mrs)


if __name__ == '__main__':
    _sys.exit(_main())

