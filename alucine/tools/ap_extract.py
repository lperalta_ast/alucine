#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ALUCINE: A set of tools for emission line fitting of astronomical IFU data
# Copyright (C) 2022  Luis Peralta de Arriba
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
``ap_extract`` file
"""


import logging as _logging
import sys as _sys
from argparse import ArgumentParser as _ArgumentParser
from os.path import basename as _basename
from os.path import splitext as _splitext

import numpy as _np
from astropy.io.ascii import read as _ascii_read
from astropy.io.fits import open as _fits_open

from ..utils.logging import add_logging_to_parser as _add_logging_to_parser
from ..utils.logging import set_logging_config as _set_logging_config
from ..utils.logging import set_warning_config as _set_warning_config


def get_parser(add_help=True):
    """
    Parser of ``ap_extract`` file
    """

    parser = _ArgumentParser(
        add_help=add_help,
        description='tool for extracting spectra from a cube into a ' +
                        'pseudo-slit')

    parser.add_argument(
        'in_filename',
        help='the name of the input FITS file with a data cube')

    parser.add_argument(
        'extraction_filename',
        help='the name of the file containing the definitions of the extractions')

    parser.add_argument(
        '--out_filename', default=None,
        help='the name to be used for the output file')
    
    parser.add_argument(
        '--two_cols', dest='two_cols', action='store_true',
        help='add second column with the same data ' +
             '(this is useful for visualizing the cube with QFitsView)')
    
    _add_logging_to_parser(parser)
    
    parser.set_defaults(func=_main)

    return parser


def _get_square_extraction(data, x, y, size):

    assert size > 0
    
    int_x = int(x)
    if float(int_x) != float(x):
        _logging.warning('Suspicious casting for x of a square extraction')
    
    int_y = int(y)
    if float(int_y) != float(y):
        _logging.warning('Suspicious casting for y of a square extraction')
    
    int_size = int(size)
    if float(int_size) != float(size):
        _logging.warning('Suspicious casting for size of a square extraction')

    # Get the limits of the aperture
    # (it is different if the size is even or odd)
    
    semisize = int_size // 2

    if int_size % 2 == 0:
        min_x, max_x = int_x - semisize + 1, int_x + semisize + 1
        min_y, max_y = int_y - semisize + 1, int_y + semisize + 1
    else:
        min_x, max_x = int_x - semisize, int_x + semisize + 1
        min_y, max_y = int_y - semisize, int_y + semisize + 1
    
    # Get a cubelet corresponding to the aperture

    extraction_data = data[:, min_y:max_y, min_x:max_x]
    
    # Collapse the cubelet in the spactial dimensions to get the spectrum of
    # the extraction
    
    extraction_spec = _np.nansum(extraction_data, axis=(1, 2))
    
    return extraction_spec


def _get_circle_extraction(data, x, y, size):

    assert size > 0
    
    # Get arrays with x and y coordinates of each pixel of the data
    
    num_wl, num_y, num_x = data.shape
    
    x_coord = _np.arange(num_x)
    y_coord = _np.arange(num_y)
    xv, yv = _np.meshgrid(x_coord, y_coord, indexing='xy')
    
    # Create a copy of the input data and remove the data farer than the half of
    # the size
    
    extraction_data = data.copy()

    for i in range(num_x):
        for j in range(num_y):
        
            dist = _np.hypot(xv[j, i] - x, yv[j, i] - y)
            
            if dist > size / 2.0:
                extraction_data[:, j, i] = _np.nan
    
    # Collapse the cube to get the extracted spectrum

    extraction_spec = _np.nansum(extraction_data, axis=(1, 2))
    
    return extraction_spec


def _get_extraction(data, x, y, extraction_type, size):

    if extraction_type == 'square':
        extraction_spec = _get_square_extraction(data, x, y, size)
    elif extraction_type == 'circle':
        extraction_spec = _get_circle_extraction(data, x, y, size)
    else:
        raise ValueError
    
    return extraction_spec


def get_extractions_from_apertures(in_filename, extraction_filename,
                                   out_filename=None, two_cols=False):
    """
    Main function of ``ap_extract`` file
    """
    
    _logging.info('getting extracted spectra defined in {} for {}...'.format(
                   extraction_filename, in_filename))

    # Read the file with the information of the extractions

    extraction_table = _ascii_read(extraction_filename)

    # Read the input cube

    hdu_list = _fits_open(in_filename)
    data = hdu_list[0].data
    num_wl, num_y, num_x = data.shape
    
    # Create an array for storing the extractions with the proper dimensions
    
    if two_cols == True:
        new_num_x = 2
    else:
        new_num_x = 1
    
    new_num_y = len(extraction_table)
    new_num_wl = num_wl
    
    new_shape = new_num_wl, new_num_y, new_num_x
    
    new_data = _np.zeros(new_shape)
    
    # For each extraction, get its spectrum and save it in the array

    for i, extraction_info in enumerate(extraction_table):
        
        x, y, extraction_type, size = extraction_info
        
        assert x < num_x
        assert y < num_y
        assert extraction_type in ['square', 'circle']
        
        extraction_spec = _get_extraction(data, x, y, extraction_type, size)
        
        new_data[:, i, 0] = extraction_spec
        
        if two_cols == True:
            new_data[:, i, 1] = extraction_spec
    
    # Create a new FITS file as the input one with the data replaced by the
    # extractions

    hdu_list[0].data = new_data
    
    if out_filename is None:
        out_filename = _splitext(_basename(in_filename))[0] + '-extractions.fits'

    hdu_list.writeto(out_filename)


def _main(opts=None):
    
    # Parse command line switches
    
    if opts is None:
        args = _sys.argv[1:]
    
        parser = get_parser()
        opts = parser.parse_args(args)
    
    # Start off the logging and silent warnings (if requested)
    
    _set_logging_config(opts.log_level)
    _set_warning_config(opts.lib_warnings)
    
    # Call the main function
    
    get_extractions_from_apertures(
       opts.in_filename, opts.extraction_filename,
       out_filename=opts.out_filename, two_cols=opts.two_cols)


if __name__ == '__main__':
    _sys.exit(_main())

