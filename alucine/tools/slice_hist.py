#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ALUCINE: A set of tools for emission line fitting of astronomical IFU data
# Copyright (C) 2022  Luis Peralta de Arriba
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
``slice_hist`` file
"""


import logging as _logging
from argparse import ArgumentParser as _ArgumentParser

import numpy as _np
import matplotlib.pyplot as _plt
from astropy.io.fits import open as _fits_open

from ..utils.logging import add_logging_to_parser as _add_logging_to_parser
from ..utils.logging import set_logging_config as _set_logging_config
from ..utils.logging import set_warning_config as _set_warning_config


def get_parser(add_help=True):
    """
    Parser of ``slice_hist`` file
    """

    parser = _ArgumentParser(
        add_help=add_help,
        description='tool for showing histograms of a parameter in an ' +
                         'ALUCINE fittings file')

    parser.add_argument('filename',
        help=('the name of the input FITS file with the results of an ' +
              'ALUCINE fitting'))
    parser.add_argument('--extname', default='FIT_PARAMS',
        help=('the name of the extension containing the parameters (e.g. ' +
              'FIT_PARAMS, SCI_PARAMS...) (default: %(default)s)'))
    parser.add_argument('--index', type=int, default=None,
        help=('the number of the parameter to be plotted ' +
              '(default: plot all the parameters sequentially)'))
    parser.add_argument('--bins', type=int, default=10,
        help=('the number of bins of the histograms (default: %(default)s)'))
    
    _add_logging_to_parser(parser)
    
    parser.set_defaults(func=_main)

    return parser


def get_histogram_from_slice(filename, extname='FIT_PARAMS', index=None,
                             bins=10):
    """
    Main function of ``slice_hist`` file
    """

    _logging.info('plotting histrogram for {}[][]...'.format(
                   filename, extname, index))
    
    with _fits_open(filename) as hdu_list:
        data = hdu_list[extname].data
    
    if index is None:
        num_index = data.shape[0]
        index_list = list(range(num_index))
    else:
        index_list = [index]
    
    for each_index in index_list:
    
        index_image = data[each_index, :, :]
        
        fig, ax = _plt.subplots()
        
        ax.hist(index_image.flatten(), bins=bins)
        
        min_value = _np.nanmin(index_image)
        max_value = _np.nanmax(index_image)
        median_value = _np.nanmedian(index_image)
        min_perc_one_sigma_value = _np.nanpercentile(
            index_image, (100.0 - 68.27) / 2.0)
        max_perc_one_sigma_value = _np.nanpercentile(
            index_image, 100.0 - (100.0 - 68.27) / 2.0)
        min_perc_two_sigma_value = _np.nanpercentile(
            index_image, (100.0 - 95.45) / 2.0)
        max_perc_two_sigma_value = _np.nanpercentile(
            index_image, 100.0 - (100.0 - 95.45) / 2.0)
        
        ax.axvline(min_value, ls='-', c='k')
        ax.axvline(max_value, ls='-', c='k')
        
        ax.axvline(median_value, ls='-.', c='k')
        
        ax.axvline(min_perc_one_sigma_value, ls='--', c='r')
        ax.axvline(max_perc_one_sigma_value, ls='--', c='r')
        
        ax.axvline(min_perc_two_sigma_value, ls=':', c='g')
        ax.axvline(max_perc_two_sigma_value, ls=':', c='g')
        
        text = ('Median: {:.2f}\n'.format(median_value) +
                'Min / Max: {:.2f} - {:.2f}\n'.format(min_value, max_value) +
                '1-$\sigma$: {:.2f} - {:.2f}\n'.format(min_perc_one_sigma_value,
                                                       max_perc_one_sigma_value) +
                '2-$\sigma$: {:.2f} - {:.2f}'.format(min_perc_two_sigma_value,
                                                     max_perc_two_sigma_value))
        
        ax.text(0.95, 0.95, text, ha='right', va='top', transform=ax.transAxes)
        
        ax.set_title('{}[{}][{}, :, :]'.format(filename, extname, each_index))
        
        _plt.show()


def _main(opts=None):

    # Parse command line switches
    
    if opts is None:
        args = _sys.argv[1:]
    
        parser = get_parser()
        opts = parser.parse_args(args)
    
    # Start off the logging and silent warnings (if requested)
    
    _set_logging_config(opts.log_level)
    _set_warning_config(opts.lib_warnings)
    
    # Call the main function
    
    get_histogram_from_slice(opts.filename, extname=opts.extname,
                             index=opts.index, bins=opts.bins)


if __name__ == '__main__':
    _sys.exit(_main())

