#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ALUCINE: A set of tools for emission line fitting of astronomical IFU data
# Copyright (C) 2022  Luis Peralta de Arriba
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
``flux_hz_to_a`` file
"""


import logging as _logging
from argparse import ArgumentParser as _ArgumentParser
from os.path import basename as _basename
from os.path import exists as _exists
from os.path import splitext as _splitext

import numpy as _np
from astropy.io.fits import open as _fits_open

from ..utils.logging import add_logging_to_parser as _add_logging_to_parser
from ..utils.logging import set_logging_config as _set_logging_config
from ..utils.logging import set_warning_config as _set_warning_config


def get_parser(add_help=True):
    """
    Parser of ``flux_hz_to_a`` file
    """

    parser = _ArgumentParser(
        add_help=add_help,
        description='tool for transforming spectra from flux per frequency ' +
                         'to flux per wavelenght')

    parser.add_argument('filename',
                        help='the name of the input FITS file with a data cube')
    
    parser.add_argument('--alt_wcs', default=None,
                        help=('alternative WCS to be considered {B,C,D...} ' +
                              '(default: primary WCS)'))
    
    _add_logging_to_parser(parser)
    
    parser.set_defaults(func=_main)

    return parser


def get_flux_a_from_hz_for_hdu(hdu, alt_wcs=None):
    """
    Aux function of ``flux_hz_to_a`` file
    """
    
    wl_ax_index = 3
    
    c_amstrong_s = 2.99792458e18

    naxis_kwd = 'NAXIS{}'.format(wl_ax_index)

    if alt_wcs is None:
        crpix_kwd = 'CRPIX{}'.format(wl_ax_index)
        crval_kwd = 'CRVAL{}'.format(wl_ax_index)
        cdelt_kwd = 'CDELT{}'.format(wl_ax_index)
        cunit_kwd = 'CUNIT{}'.format(wl_ax_index)
        ctype_kwd = 'CTYPE{}'.format(wl_ax_index)
    else:
        crpix_kwd = 'CRPIX{}{}'.format(wl_ax_index, alt_wcs)
        crval_kwd = 'CRVAL{}{}'.format(wl_ax_index, alt_wcs)
        cdelt_kwd = 'CDELT{}{}'.format(wl_ax_index, alt_wcs)
        cunit_kwd = 'CUNIT{}{}'.format(wl_ax_index, alt_wcs)
        ctype_kwd = 'CTYPE{}{}'.format(wl_ax_index, alt_wcs)

    data = hdu.data
    
    bunit = hdu.header['BUNIT']
    num_pix = hdu.header[naxis_kwd]
    crpix = hdu.header[crpix_kwd]
    crval = hdu.header[crval_kwd]
    cdelt = hdu.header[cdelt_kwd]
    cunit = hdu.header[cunit_kwd]
    ctype = hdu.header[ctype_kwd]
    
    assert bunit == 'Jy'
    assert cunit == 'Angstrom'
    assert ctype == 'WAVE' or ctype == 'AWAV'
    
    if ctype == 'AWAV':
        _logging.warning('AWAV implementation should be checked')

    obs_wl_array = crval + cdelt * (_np.arange(num_pix) - (crpix - 1))
    
    new_bunit = 'erg * s^-1 * cm^-2 * Angstrom^-1'
    
    per_freq_to_wl_array = (1e-23 * c_amstrong_s) / (obs_wl_array**2)
    
    new_data = data * per_freq_to_wl_array[:, _np.newaxis, _np.newaxis]
    
    med_value = _np.median(new_data[new_data > 0])
    exp_value = int(_np.floor(_np.log10(med_value)))
    
    _logging.info('Exponent value: 10^{}'.format(exp_value))
    
    if exp_value != 0:
        new_data /= 10**exp_value
        new_bunit = '10^{} * '.format(exp_value) + new_bunit
    
    hdu.data = new_data
    hdu.header['BUNIT'] = new_bunit
    
    return hdu


def get_flux_a_from_hz(filename, alt_wcs=None):
    """
    Main function of ``flux_hz_to_a`` file
    """
    
    _logging.info('getting flux per angstrom from hertz for {}...'.format(
                   filename))

    with _fits_open(filename, mode='readonly') as hdu_list:
        
        hdu_list[0] = get_flux_a_from_hz_for_hdu(hdu_list[0], alt_wcs=None)
        
        out_filename = '_wl'.join(_splitext(_basename(filename)))
        if not _exists(out_filename):
            hdu_list.writeto(out_filename)
            _logging.info('output file written in {}'.format(out_filename))
        else:
            _logging.error(
                'output file {} already exists: nothing done'.format(
                    out_filename))


def _main(opts=None):

    # Parse command line switches
    
    if opts is None:
        args = _sys.argv[1:]
    
        parser = get_parser()
        opts = parser.parse_args(args)
    
    # Start off the logging and silent warnings (if requested)
    
    _set_logging_config(opts.log_level)
    _set_warning_config(opts.lib_warnings)
    
    # Call the main function
    
    get_flux_a_from_hz(opts.filename, alt_wcs=opts.alt_wcs)


if __name__ == '__main__':
    _sys.exit(_main())

