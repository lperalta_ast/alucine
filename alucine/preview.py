#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ALUCINE: A set of tools for emission line fitting of astronomical IFU data
# Copyright (C) 2022  Luis Peralta de Arriba
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
``preview`` file
"""


import logging as _logging
import sys as _sys
from argparse import ArgumentParser as _ArgumentParser

import numpy as _np
import astropy.units as _u
import matplotlib.pyplot as _plt
from matplotlib.colors import LogNorm as _LogNorm

from .utils.io import read_control_file as _read_control_file
from .utils.io import read_cube as _read_cube
from .utils.logging import add_logging_to_parser as _add_logging_to_parser
from .utils.logging import set_logging_config as _set_logging_config
from .utils.logging import set_warning_config as _set_warning_config


def get_parser(add_help=True):
    """
    Parser of ``preview`` file
    """

    parser = _ArgumentParser(add_help=add_help,
        description='ALUCINE step 1: previewing')

    parser.add_argument('control_file',
                        help='a YAML file with the control instructions')

    group_interactive = parser.add_mutually_exclusive_group()
    group_interactive.add_argument(
        '--interactive', dest='interactive', action='store_true',
        help='view the fittings interactively')
    group_interactive.add_argument('--save', dest='interactive', action='store_false',
        help='save the fittings into a file')
    group_interactive.set_defaults(interactive=True)
    
    _add_logging_to_parser(parser)
    
    parser.set_defaults(func=_main)

    return parser


#def _plot_fits(vect_ax, multi_fit_cubelet_result, fit_model, i, j,
#               num_comp=None, best_fit_aon=None,
#               draw_init=True, draw_text=False):

#    result_dict = multi_fit_cubelet_result[j, i]

#    max_num_components = result_dict['max_num_components']
#    assert max_num_components == len(vect_ax)
#    
#    for i in range(max_num_components):
#        
#        j = i + 1
#        
#        vect_ax[i].clear()
#        
#        vect_ax[i].plot(result_dict['xdata'], result_dict['ydata'],
#                        color='k', ds='steps-mid')

#        vect_ax[i].plot(result_dict['xdata'], result_dict[j]['residual_data'],
#                        color='orange', ds='steps-mid')

#        if draw_init is True:
#            vect_ax[i].plot(result_dict['xdata'], result_dict[j]['init_ydata'],
#                            color='g', lw=3, ls=':')

#        vect_ax[i].plot(result_dict['xdata'], result_dict[j]['fit_ydata'],
#                        color='r', lw=2)
#        
#        if draw_text is True:

#            if i == (max_num_components - 1):
#                vect_ax[i].text(1.03, 0.5,
#                                'noise = {:.3g}'.format(result_dict['noise']),
#                                va='center', ha='left',
#                                transform=vect_ax[i].transAxes, rotation=90)

#            text = (
#                '$\chi^2 = {:.3g}$\n'.format(
#                    result_dict[j]['goodness_of_fit']) +
#                '$max(res) = {:.3g}$\n'.format(
#                    _np.max(result_dict[j]['residual_data'])) +
#                '$mean(res) = {:.3g}$\n'.format(
#                    _np.mean(result_dict[j]['residual_data'])) +
#                '$std(res) = {:.3g}$\n'.format(
#                    _np.std(result_dict[j]['residual_data'])))

#            vect_ax[i].text(0.97, 0.95, text,
#                            va='top', ha='right',
#                            transform=vect_ax[i].transAxes)
#        
#        _plot_components(vect_ax[i], fit_model, result_dict['xdata'],
#                         result_dict[j]['fit_params'])

#        # Draw a region to indicate the noise
#    
#        vect_ax[i].axhspan(-result_dict['noise'], result_dict['noise'],
#                           color='k', alpha=0.25)

#        # Plot a line with the AoN limit if provided

#        if best_fit_aon is not None:
#            vect_ax[i].axhline(best_fit_aon * result_dict['noise'],
#                               color='purple', ls=':')

#    # Set the limits for the axis
#    
#    xlim = [result_dict['xdata'][0], result_dict['xdata'][-1]]
#    ylim = vect_ax[0].get_ylim()
#    
#    for i in range(len(vect_ax)):
#        vect_ax[i].set_xlim(xlim)
#        vect_ax[i].set_ylim(ylim)
#    
#        if i > 0:
#            vect_ax[i].set_yticks([])

#    # Set the background colour if the selected number of components is provided
#        
#    if num_comp is not None:
#        for i in range(max_num_components):
#            if i != (num_comp - 1):
#                vect_ax[i].set_facecolor('lightgrey')
#            else:
#                vect_ax[i].set_facecolor('white')


#def _get_dict_ax(fig, num_cols_first_row, num_components, component_num_params):

#    nrows = 1 + num_components + 1
#    
#    dict_ax = {}

#    for row in range(nrows):

#        if row == 0:
#            ncols = num_cols_first_row
#        elif row == nrows - 1:
#            ncols = num_components
#        else:
#            ncols = component_num_params

#        for col in range(ncols):
#            dict_index = (row, col)
#            subplot_index = row * ncols + col + 1
#            dict_ax[dict_index] = fig.add_subplot(
#                nrows, ncols, (subplot_index, subplot_index))

#    return dict_ax


#def _plot_array_on_ax(fig, ax, array, title=None, type_param=None,
#                      line_rf_wl=None):

#    if type_param in ['A', 'I']:
#        vmax = _np.nanmax(array)
#        vmin = vmax / 1e2
#        cset = ax.imshow(array, origin='lower',
#                         norm=_LogNorm(vmin=vmin, vmax=vmax))
#    elif type_param == 'c':
#        if type_param is not None:
#            vshift = _np.nanpercentile(_np.abs(array - line_rf_wl), 95.0)
#            vmin = line_rf_wl - vshift
#            vmax = line_rf_wl + vshift
#        else:
#            vmin = _np.nanpercentile(array, 5.0)
#            vmax = _np.nanpercentile(array, 95.0)
#        cset = ax.imshow(
#            array, origin='lower', vmin=vmin, vmax=vmax,
#            cmap=sauron_cmap.sauron_purple_brown_cmap)
#    elif type_param == 'v':
#        vmax = _np.nanpercentile(_np.abs(array), 95.0)
#        vmin = -vmax
#        cset = ax.imshow(array, origin='lower', vmin=vmin, vmax=vmax,
#                         cmap=sauron_cmap.sauron_purple_brown_cmap)
#    elif type_param in ['w', 'd']:
#        vmin = _np.nanmin(array)
#        vmax = _np.nanpercentile(array, 95.0)
#        cset = ax.imshow(array, origin='lower',
#                         cmap=sauron_cmap.sauron_purple_brown_cmap)
#    else:
#        cset = ax.imshow(array, origin='lower')

#    ax.set_xticks([])
#    ax.set_yticks([])

#    cbar = fig.colorbar(cset, ax=ax)

#    if title is not None:
#        ax.set_title(title)

#    if type_param in ['c', 'w']:
#        cbar.set_label('$\AA$')
#    elif type_param in ['v', 'd']:
#        cbar.set_label('km / s')


def _preview(obs_wl_array, collapsed_spec, spec_at_max, redshift,
             line_rf_wl, min_rf_wl, max_rf_wl, cont_rf_window_list,
             output_filename, suptitle=None, interactive=True):
    
    # Create the figure with the subplots
    
    figsize = [3.0 * 6.4, 2.0 * 5.8]

    fig, vect_ax = _plt.subplots(nrows=2, figsize=figsize, sharex=True)
    
    vect_ax[0].plot(obs_wl_array, collapsed_spec, color='k', ds='steps-mid',
                    label='Mean spectrum')

    vect_ax[1].plot(obs_wl_array, spec_at_max, color='k', ds='steps-mid',
                    label='Spectrum at maximum')

    for ax in vect_ax:
        ax.axvline((1.0 + redshift) * line_rf_wl, ls=':', color='b')
        ax.axvline((1.0 + redshift) * min_rf_wl, ls='-.', color='r')
        ax.axvline((1.0 + redshift) * max_rf_wl, ls='-.', color='r')

        for cont_rf_window in cont_rf_window_list:
            ax.axvspan((1.0 + redshift) * cont_rf_window[0],
                       (1.0 + redshift) * cont_rf_window[1],
                       color='grey', alpha=0.5)

        ax.legend()

    lower_obs_wl = ((1.0 + redshift) *
                    _np.min([min_rf_wl, _np.min(cont_rf_window_list)]))
    upper_obs_wl = ((1.0 + redshift) *
                    _np.max([min_rf_wl, _np.max(cont_rf_window_list)]))

    margin = 0.1 * (upper_obs_wl - lower_obs_wl)

    min_xlim = lower_obs_wl - margin
    max_xlim = upper_obs_wl + margin

    vect_ax[0].set_xlim([min_xlim, max_xlim])
    
    if suptitle is not None:
        fig.suptitle(suptitle)

    if interactive is True:
        _plt.show()
    else:
        fig.savefig(output_filename)


def get_preview(control_file, interactive=True):
    """
    Main function of ``preview`` file
    """
    
    _logging.info('starting preview step for control file {}...'.format(
                   control_file))

    # Read the YAML control file

    control_dict = _read_control_file(control_file)

    # De-reference the information from the control file to be used in this tool

    cube_filename = control_dict['cube_filename']
    output_prefix = control_dict['output_prefix']
    redshift = control_dict['redshift']
    alt_wcs = control_dict['alt_wcs']
    all_cubelets_dict = control_dict['cubelets']

    # Read the cube

    cube = _read_cube(cube_filename, alt_wcs=alt_wcs)
    cube.allow_huge_operations=True

    # Get the coordinates of the cube along the spectral axis

    obs_wl_array_with_unit, _, _ = cube.world[:, 0, 0]
    
    obs_wl_array = obs_wl_array_with_unit.value
    assert obs_wl_array_with_unit.unit == _u.AA

    # Get the spectra for the previews

    collapsed_image = cube.mean(axis=0)
    collapsed_spec = cube.mean(axis=(1,2))

    max_index = _np.unravel_index(_np.argmax(collapsed_image),
                                 collapsed_image.shape)

    spec_at_max = cube[:, max_index[0], max_index[1]]
    
    # Fit each cubelet as requested

    for cubelet_name, cubelet_dict in all_cubelets_dict.items():
        
        output_filename = '{}-{}.png'.format(output_prefix, cubelet_name)

        line_rf_wl = cubelet_dict['line_rf_wl']
        min_rf_wl = cubelet_dict['min_rf_wl']
        max_rf_wl = cubelet_dict['max_rf_wl']
        cont_rf_window_list = cubelet_dict['cont_rf_window_list']

        suptitle = '{} - {}'.format(output_prefix, cubelet_name)
        
        _preview(obs_wl_array, collapsed_spec, spec_at_max, redshift,
                 line_rf_wl, min_rf_wl, max_rf_wl, cont_rf_window_list,
                 output_filename, suptitle=suptitle, interactive=interactive)


def _main(opts=None):

    # Parse command line switches
    
    if opts is None:
        args = _sys.argv[1:]
    
        parser = get_parser()
        opts = parser.parse_args(args)
    
    # Start off the logging and silent warnings (if requested)
    
    _set_logging_config(opts.log_level)
    _set_warning_config(opts.lib_warnings)
    
    # Call the main function
    
    get_preview(opts.control_file, interactive=opts.interactive)


if __name__ == '__main__':
    _sys.exit(_main())

