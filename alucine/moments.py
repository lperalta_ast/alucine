#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ALUCINE: A set of tools for emission line fitting of astronomical IFU data
# Copyright (C) 2022  Luis Peralta de Arriba
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
``moments`` file
"""


import logging as _logging
import sys as _sys
from argparse import ArgumentParser as _ArgumentParser

import astropy.units as _u
from astropy.io import fits as _fits
from spectral_cube import SpectralCube as _SpectralCube

from .utils.io import read_control_file as _read_control_file
from .utils.logging import add_logging_to_parser as _add_logging_to_parser
from .utils.logging import set_logging_config as _set_logging_config
from .utils.logging import set_warning_config as _set_warning_config
from .utils.wcs import remove_kwds_of_third_axis as _remove_kwds_of_third_axis


def get_parser(add_help=True):
    """
    Parser of ``moments`` file
    """

    parser = _ArgumentParser(add_help=add_help,
        description='ALUCINE step 3: computing moments of each cubelet')

    parser.add_argument('control_file',
                        help='a YAML file with the control instructions')
    
    _add_logging_to_parser(parser)
    
    parser.set_defaults(func=_main)

    return parser


def _get_moments_for_one_cubelet(
        cubelet_filename, line_rf_wl, redshift, moments_filename,
        velocity_convention='optical'):

    # Create a spectral cube from a FITS file

    with _fits.open(cubelet_filename, mode='readonly') as hdu_list:
        hdr = hdu_list[0].header.copy()
        cubelet = _SpectralCube.read(hdu_list, hdu=0)

    # Set the observed wavelenght of the line

    line_obs_wl = (1.0 + redshift) * line_rf_wl

    # Get a spectral cubelet in the velocity space

    speed_cubelet = cubelet.with_spectral_unit(
        _u.km/_u.s, velocity_convention=velocity_convention,
        rest_value=line_obs_wl*_u.AA)

    # Compute the moments

    data_list = [speed_cubelet.moment(order=i).array for i in range(3)]

    # Save the moments into a FITS file

    _remove_kwds_of_third_axis(hdr)

    primary_hdu = _fits.PrimaryHDU()

    list_of_hdu = [_fits.ImageHDU(data=data_list[i], header=hdr,
                                  name='MOMENT{}'.format(i))
                   for i in range(3)]

    list_of_hdu = [primary_hdu] + list_of_hdu

    hdu_list = _fits.HDUList(list_of_hdu)

    hdu_list.writeto(moments_filename, overwrite=True)


def get_moments(control_file, velocity_convention='optical'):
    """
    Main function of ``moments`` file
    """

    _logging.info('starting moments step for control file {}...'.format(
                   control_file))

    # Read the YAML control file

    control_dict = _read_control_file(control_file)
    
    # De-reference the information from the control file to be used in this tool

    output_prefix = control_dict['output_prefix']
    redshift = control_dict['redshift']
    all_cubelets_dict = control_dict['cubelets']

    # Compute the moments for each cubelet

    for cubelet_name, cubelet_dict in all_cubelets_dict.items():
        
        cubelet_filename = '{}-{}.fits'.format(output_prefix, cubelet_name)
        moments_filename = '{}-{}-moments.fits'.format(output_prefix,
                                                       cubelet_name)

        line_rf_wl = cubelet_dict['line_rf_wl']

        _logging.info('creating moments file {}...'.format(moments_filename))

        _get_moments_for_one_cubelet(
            cubelet_filename, line_rf_wl, redshift, moments_filename,
            velocity_convention=velocity_convention)


def _main(opts=None):

    # Parse command line switches
    
    if opts is None:
        args = _sys.argv[1:]
    
        parser = get_parser()
        opts = parser.parse_args(args)
    
    # Start off the logging and silent warnings (if requested)
    
    _set_logging_config(opts.log_level)
    _set_warning_config(opts.lib_warnings)
    
    # Call the main function
    
    get_moments(opts.control_file)


if __name__ == '__main__':
    _sys.exit(_main())

