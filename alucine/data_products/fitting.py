#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ALUCINE: A set of tools for emission line fitting of astronomical IFU data
# Copyright (C) 2022  Luis Peralta de Arriba
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
``fitting`` file: it contains a class for a fitting data product
"""


import numpy as _np
from astropy.io import fits as _fits

from ..models import get_model_from_name as _get_model_from_name


class Fitting:
    """
    Class for easily interacting with fitting data product
    """
    
    def __init__(self, filename):
        
        self.filename = filename
        self.hdu_list = _fits.open(filename)
        self.hdr = self.hdu_list['SPEC'].header
        self.fit_model = _get_model_from_name(self.hdr['FITMODEL'])
        
        self.num_params = self.hdu_list['FIT_PARAMS'].header['NAXIS3']
        self.baseline_num_params = self.fit_model.get_baseline_num_params()
        self.component_num_params = self.fit_model.get_component_num_params()
        self.max_num_components = (
            (self.num_params - self.baseline_num_params) //
            self.component_num_params)

    def get_obs_wl(self):
        
        obs_wl = (self.hdr['CRVAL3'] + self.hdr['CDELT3'] *
                  (_np.arange(self.hdr['NAXIS3']) + 1 - self.hdr['CRPIX3']))
        
        return obs_wl

    def get_rf_wl(self):

        obs_wl = self.get_obs_wl()
        
        rf_wl = obs_wl / (1.0 + self.hdr['REDSHIFT'])
        
        return rf_wl

    def get_spaxel_spec(self, spaxel_i, spaxel_j):

        spaxel_spec = self.hdu_list['SPEC'].data[:, spaxel_j, spaxel_i]
        
        return spaxel_spec

    def get_spaxel_fit_spec(self, spaxel_i, spaxel_j):

        spaxel_fit_spec = self.hdu_list['FIT_SPEC'].data[:, spaxel_j, spaxel_i]
        
        return spaxel_fit_spec
    
    def get_spaxel_fit_params(self, spaxel_i, spaxel_j):
        
        spaxel_fit_params = self.hdu_list['FIT_PARAMS'].data[:, spaxel_j, spaxel_i]
        
        return spaxel_fit_params

    def get_spaxel_component(self, spaxel_i, spaxel_j, comp_index):
    
        xdata = self.get_rf_wl()
        
        fit_params = self.get_spaxel_fit_params(spaxel_i, spaxel_j)

        # Get the baseline

        baseline_params = fit_params[:self.baseline_num_params]
        baseline_ydata = self.fit_model.fit_function_for_baseline(
                             xdata, *baseline_params)
        
        # Plot the components
        
        comp_param_ini = (self.baseline_num_params +
                          (comp_index - 1) * self.component_num_params)
        comp_param_end = (self.baseline_num_params +
                          comp_index * self.component_num_params)
        
        component_params = fit_params[comp_param_ini:comp_param_end]

        component_ydata = self.fit_model.fit_function_for_component(
            xdata, *component_params)

        spaxel_component = baseline_ydata + component_ydata
        
        return spaxel_component

    def get_spaxel_component_list(self, spaxel_i, spaxel_j):
        
        spaxel_component_list = [
            self.get_spaxel_component(spaxel_i, spaxel_j, k + 1)
                for k in range(self.max_num_components)]
        
        return spaxel_component_list
    
    def get_fit_params_cube(self):
        
        fit_params_cube = self.hdu_list['FIT_PARAMS'].data
        
        return fit_params_cube
    
    def get_fit_param_image(self, index1, index2=None):
        
        if index2 is None:
            assert (index1 >= 0) and (index1 < self.num_params)
            index = index1
        else:
            assert (index1 > 0) and (index1 <= self.max_num_components)
            assert (index2 >= 0) and (index2 < self.component_num_params)
            index = (self.fit_model.baseline_num_params +
                     (index1 - 1) * self.component_num_params + index2)
        
        fit_param_image = self.hdu_list['FIT_PARAMS'].data[index, :, :]
        
        return fit_param_image
    
    def get_sci_params_cube(self):
        
        sci_params_cube = self.hdu_list['SCI_PARAMS'].data
        
        return sci_params_cube
    
    def get_sci_param_image(self, index1, index2=None):
        
        if index2 is None:
            assert (index1 >= 0) and (index1 < self.num_params)
            index = index1
        else:
            assert (index1 > 0) and (index1 <= self.max_num_components)
            assert (index2 >= 0) and (index2 < self.component_num_params)
            index = (self.fit_model.baseline_num_params +
                     (index1 - 1) * self.component_num_params + index2)
        
        sci_param_image = self.hdu_list['SCI_PARAMS'].data[index, :, :]
        
        return sci_param_image
    
    def get_peak_to_fit_image(self):
        
        peak_to_fit_image = self.hdu_list['PEAK_TO_FIT'].data
        
        return peak_to_fit_image
    
    def get_noise_image(self):
        
        noise_image = self.hdu_list['NOISE'].data
        
        return noise_image
    
    def get_chi_image(self):
        
        noise_image = self.hdu_list['CHI'].data
        
        return noise_image
    
    def get_component_name_fit_params(self):
        return self.fit_model.component_name_fit_params
    
    def get_component_name_sci_params(self):
        return self.fit_model.component_name_sci_params
    
    def get_max_num_components(self):
        return self.max_num_components

