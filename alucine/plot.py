#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ALUCINE: A set of tools for emission line fitting of astronomical IFU data
# Copyright (C) 2022  Luis Peralta de Arriba
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
``plot`` file
"""


import logging as _logging
import sys as _sys
from argparse import ArgumentParser as _ArgumentParser
from os.path import splitext as _splitext

import numpy as _np
import matplotlib.pyplot as _plt
from matplotlib.colors import LogNorm as _LogNorm
from astropy.io.fits import open as _fits_open

from .models import get_model_from_name as _get_model_from_name
from .utils.cmaps.sauron import sauron_purple_brown_cmap as _sauron_purple_brown_cmap
from .utils.io import read_control_file as _read_control_file
from .utils.io import read_multifitting_from_hdf5 as _read_multifitting_from_hdf5
from .utils.logging import add_logging_to_parser as _add_logging_to_parser
from .utils.logging import set_logging_config as _set_logging_config
from .utils.logging import set_warning_config as _set_warning_config
from .utils.plotting import ClickZoomFigUpdater as _ClickZoomFigUpdater


def get_parser(add_help=True):
    """
    Parser of ``plot`` file
    """

    parser = _ArgumentParser(
        add_help=add_help,
        description='ALUCINE step 5: plotting the fits')

    parser.add_argument('control_file',
                        help='a YAML file with the control instructions')

    parser.add_argument('--type_params', default='sci', choices=['sci', 'fit'],
                        help=('the type of parameters to be displayed ' +
                              '(default: %(default)s)'))

    parser.add_argument('--vperc', type=float, default=5.0,
                        help=('percentil to be used in the colour bars ' +
                              '(default: %(default)s)'))
    
    parser.add_argument('--add_v_sys', action='store_true',
                        help='add systemic velocity to the velocity plots')
    
    group_interactive = parser.add_mutually_exclusive_group()
    group_interactive.add_argument(
        '--interactive', dest='interactive', action='store_true',
        help='view the fittings interactively')
    group_interactive.add_argument('--save', dest='interactive', action='store_false',
        help='save the fittings into a file')
    group_interactive.set_defaults(interactive=True)

    
    _add_logging_to_parser(parser)
    
    parser.set_defaults(func=_main)

    return parser


def _plot_components(ax, fit_model, xdata, fit_params, color=['b', 'c', 'purple']):

    # Guess the number of parameters for the baseline and for each component

    num_params = len(fit_params)

    baseline_num_params = fit_model.get_baseline_num_params()
    component_num_params = fit_model.get_component_num_params()
    
    num_components = (num_params - baseline_num_params) // component_num_params

    # Get the baseline

    baseline_params = fit_params[:baseline_num_params]
    baseline_ydata = fit_model.fit_function_for_baseline(xdata,
                                                         *baseline_params)
    
    if type(color) is not list:
        color_list = [color]
    else:
        color_list = color
    
    for i in range(num_components):
        comp_param_ini = baseline_num_params + i * component_num_params
        comp_param_end = baseline_num_params + (i + 1) * component_num_params
        
        component_params = fit_params[comp_param_ini:comp_param_end]

        component_ydata = fit_model.fit_function_for_component(
            xdata, *component_params)

        ydata = baseline_ydata + component_ydata
        
        ax.plot(xdata, ydata, color=color_list[i % len(color_list)])


def _updater_function(fig, vect_ax, data, x, y):

    multi_fit_cubelet_result = data['multi_fit_cubelet_result']
    num_comp = data['num_comp_image'][y, x]
    best_fit_aon = data['best_fit_aon']
    fit_model = data['fit_model']
    
    _plot_fits(vect_ax, multi_fit_cubelet_result, fit_model, x, y,
               num_comp=num_comp, best_fit_aon=best_fit_aon)

    vect_ax[-1].text(1.0, -0.15, 'x={} y={}'.format(x, y),
                     va='top', ha='right', transform=vect_ax[-1].transAxes)

    fig.canvas.draw()


def _plot_fits(vect_ax, multi_fit_cubelet_result, fit_model, i, j,
               num_comp=None, best_fit_aon=None,
               draw_init=True, draw_text=False):

    result_dict = multi_fit_cubelet_result[j, i]

    max_num_components = result_dict['max_num_components']
    assert max_num_components == len(vect_ax)
    
    xlim = vect_ax[0].get_xlim()
    overplotting_flag = (len(vect_ax[0].get_lines()) != 0)
    
    for i in range(max_num_components):
        
        j = i + 1
        
        vect_ax[i].clear()
        
        vect_ax[i].plot(result_dict['xdata'], result_dict['ydata'],
                        color='k', ds='steps-mid')

        vect_ax[i].plot(result_dict['xdata'], result_dict[j]['residual_data'],
                        color='orange', ds='steps-mid')

        if draw_init is True:
            vect_ax[i].plot(result_dict['xdata'], result_dict[j]['init_ydata'],
                            color='g', lw=3, ls=':')

        vect_ax[i].plot(result_dict['xdata'], result_dict[j]['fit_ydata'],
                        color='r', lw=2)
        
        if draw_text is True:

            if i == (max_num_components - 1):
                vect_ax[i].text(1.03, 0.5,
                                'noise = {:.3g}'.format(result_dict['noise']),
                                va='center', ha='left',
                                transform=vect_ax[i].transAxes, rotation=90)

            text = (
                '$\chi^2 = {:.3g}$\n'.format(
                    result_dict[j]['goodness_of_fit']) +
                '$max(res) = {:.3g}$\n'.format(
                    _np.max(result_dict[j]['residual_data'])) +
                '$mean(res) = {:.3g}$\n'.format(
                    _np.mean(result_dict[j]['residual_data'])) +
                '$std(res) = {:.3g}$\n'.format(
                    _np.std(result_dict[j]['residual_data'])))

            vect_ax[i].text(0.97, 0.95, text,
                            va='top', ha='right',
                            transform=vect_ax[i].transAxes)
        
        _plot_components(vect_ax[i], fit_model, result_dict['xdata'],
                         result_dict[j]['fit_params'])

        # Draw a region to indicate the noise
    
        vect_ax[i].axhspan(-result_dict['noise'], result_dict['noise'],
                           color='k', alpha=0.25)

        # Plot a line with the AoN limit if provided

        if best_fit_aon is not None:
            vect_ax[i].axhline(best_fit_aon * result_dict['noise'],
                               color='purple', ls=':')

    # Set the limits for the axis
    
    if overplotting_flag == False:
        xlim = [result_dict['xdata'][0], result_dict['xdata'][-1]]
    ylim = vect_ax[0].get_ylim()
    
    for i in range(len(vect_ax)):
        vect_ax[i].set_xlim(xlim)
        vect_ax[i].set_ylim(ylim)
    
        if i > 0:
            vect_ax[i].set_yticks([])

    # Set the background colour if the selected number of components is provided
        
    if num_comp is not None:
        for i in range(max_num_components):
            if i != (num_comp - 1):
                vect_ax[i].set_facecolor('lightgrey')
            else:
                vect_ax[i].set_facecolor('white')


def _get_dict_ax(fig, num_cols_first_row, num_components, component_num_params):

    nrows = 1 + num_components + 1
    
    dict_ax = {}

    for row in range(nrows):

        if row == 0:
            ncols = num_cols_first_row
        elif row == nrows - 1:
            ncols = num_components
        else:
            ncols = component_num_params

        for col in range(ncols):
            dict_index = (row, col)
            subplot_index = row * ncols + col + 1
            dict_ax[dict_index] = fig.add_subplot(
                nrows, ncols, (subplot_index, subplot_index))

    return dict_ax


def _plot_array_on_ax(fig, ax, array, title=None, type_param=None,
                      line_rf_wl=None, vperc=5.0, add_v_sys=False,
                      redshift=0.0):

    if type_param in ['A', 'I']:
        vmax = _np.nanmax(array)
        vmin = vmax / 1e2
        if _np.isnan(vmax): vmin, vmax = 1.0, 10.0
        cset = ax.imshow(array, origin='lower',
                         norm=_LogNorm(vmin=vmin, vmax=vmax))
        extend = 'both'
    elif type_param == 'c':
        if type_param is not None:
            vshift = _np.nanpercentile(_np.abs(array - line_rf_wl),
                                      100.0 - vperc)
            vmin = line_rf_wl - vshift
            vmax = line_rf_wl + vshift
        else:
            vmin = _np.nanpercentile(array, vperc)
            vmax = _np.nanpercentile(array, 100.0 - vperc)
        if _np.isnan(vmax): vmin, vmax = 1.0, 10.0
        cset = ax.imshow(
            array, origin='lower', vmin=vmin, vmax=vmax,
            cmap=_sauron_purple_brown_cmap)
        extend = 'both'
    elif type_param == 'v':
        if add_v_sys is False:
            v_sys = 0.0
        else:
            c_km_s = 299792.458
            v_sys = redshift * c_km_s
        
        vmax = _np.nanpercentile(_np.abs(array), 100.0 - vperc)
        vmin = -vmax
        
        aux_array = array + v_sys
        vmax += v_sys
        vmin += v_sys
        
        if _np.isnan(vmax): vmin, vmax = -1.0, 1.0
        cset = ax.imshow(aux_array, origin='lower', vmin=vmin, vmax=vmax,
                         cmap=_sauron_purple_brown_cmap)
        extend = 'both'
    elif type_param in ['w', 'd']:
        vmin = _np.nanmin(array)
        vmax = _np.nanpercentile(array, 100.0 - vperc)
        if _np.isnan(vmax): vmin, vmax = 1.0, 10.0
        cset = ax.imshow(array, origin='lower',
                         cmap=_sauron_purple_brown_cmap)
        extend = 'both'
    else:
        cset = ax.imshow(array, origin='lower')
        extend = 'neither'

    ax.set_xticks([])
    ax.set_yticks([])

    cbar = fig.colorbar(cset, extend=extend, ax=ax)

    if title is not None:
        ax.set_title(title)

    if type_param in ['c', 'w']:
        cbar.set_label('$\AA$')
    elif type_param in ['v', 'd']:
        cbar.set_label('km / s')


def _view_fits(fittings_filename, best_fit_filename, fit_model,
               suptitle=None, best_fit_aon=None, type_params='sci',
               line_rf_wl=None, interactive=True, vperc=5.0, add_v_sys=False,
               redshift=0.0):

    # Read the file with all the fits

    multi_fit_cubelet_result = _read_multifitting_from_hdf5(
        fittings_filename, with_root_attrs=False)

    # Read the file with best fits

    with _fits_open(best_fit_filename, mode='readonly') as hdu_list:
        spec_cube = hdu_list['SPEC'].data
        fit_spec_cube = hdu_list['FIT_SPEC'].data
        sci_params_cube = hdu_list['SCI_PARAMS'].data
        fit_params_cube = hdu_list['FIT_PARAMS'].data
        
        num_comp_image = hdu_list['NUM_COMP'].data
        peak_to_fit_image = hdu_list['PEAK_TO_FIT'].data
        noise_image = hdu_list['NOISE'].data
        chi_image = hdu_list['CHI'].data

    # Get the collapsed image from the cube

    collapsed_image = _np.nansum(spec_cube, axis=0)
    residual_image = _np.nansum(spec_cube - fit_spec_cube, axis=0)

    # Guess the number of parameters in the fits

    if type_params == 'sci':
        params_cube = sci_params_cube
        component_type_params = fit_model.get_component_type_sci_params()
        component_name_params = fit_model.get_component_name_sci_params()
    elif type_params == 'fit':
        params_cube = fit_params_cube
        component_type_params = fit_model.get_component_type_fit_params()
        component_name_params = fit_model.get_component_name_fit_params()
    else:
        raise ValueError

    num_params, _, _ = params_cube.shape
    
    # Guess the number of parameters for the baseline and for each component

    baseline_num_params = fit_model.get_baseline_num_params()
    component_num_params = fit_model.get_component_num_params()

    num_components = (num_params - baseline_num_params) // component_num_params

    # Create the figure with the subplots
    
    nrows = 2 + num_components

    figsize = [3.0 * 6.4, 2.0 * 5.8]

    fig = _plt.figure(figsize=figsize)

    num_cols_first_row = 3

    dict_ax = _get_dict_ax(
        fig, num_cols_first_row, num_components, component_num_params)

    # Plot the first row of the grid

    _plot_array_on_ax(fig, dict_ax[(0, 0)], collapsed_image,
                      title='Collapsed image', vperc=vperc)
    _plot_array_on_ax(fig, dict_ax[0, 1], num_comp_image,
                      title='Number of components', vperc=vperc)
    _plot_array_on_ax(fig, dict_ax[0, 2], peak_to_fit_image,
                      title='Peak to fit', vperc=vperc)

    # Plot the following rows with the information for each component

    for i, j in dict_ax.keys():
        if (i >= 1) and (i <= num_components):

            cube_index = (baseline_num_params +
                          (i - 1) * component_num_params + j)
            param_image = params_cube[cube_index, :, :]

            name_param = component_name_params[j]
            title = '{}'.format(name_param)

            type_param = component_type_params[j]
            _plot_array_on_ax(fig, dict_ax[i, j], param_image, title=title,
                              type_param=type_param, line_rf_wl=line_rf_wl,
                              vperc=vperc, add_v_sys=add_v_sys,
                              redshift=redshift)

            if (j == 0) and (num_components > 1):
                dict_ax[i, j].text(
                    -0.15, 0.5, 'Component #{}'.format(i),
                    va='center', ha='right',
                    transform=dict_ax[i, j].transAxes, rotation=90)


    # Plot the spectra at the bottom

    click_zoom_vect_ax = [dict_ax[key] for key in dict_ax.keys()
                          if key[0] <= num_components]
    updated_vect_ax = [dict_ax[key] for key in dict_ax.keys()
                       if key[0] > num_components]

    data = {'multi_fit_cubelet_result': multi_fit_cubelet_result,
            'num_comp_image': num_comp_image, 'best_fit_aon': best_fit_aon,
            'fit_model': fit_model}

    updater = _ClickZoomFigUpdater(
        fig, click_zoom_vect_ax, updated_vect_ax, _updater_function, data)
    
    restart_xlim = [data['multi_fit_cubelet_result'][0, 0]['xdata'][0],
                    data['multi_fit_cubelet_result'][0, 0]['xdata'][-1]]
    
    def _onmiddleclick(event):
        if event.button == 2:
            for ax in updated_vect_ax:
                ax.set_xlim(restart_xlim)
            fig.canvas.draw()
     
    fig.canvas.mpl_connect('button_press_event', _onmiddleclick)
    
    if suptitle is not None:
        fig.suptitle(suptitle)

    if interactive is True:
        _plt.show()
    else:
        output_filename = _splitext(fittings_filename)[0] + '.png'
        fig.savefig(output_filename)


def get_plot(control_file, type_params='sci', vperc=5.0, add_v_sys=False,
             interactive=True):
    """
    Main function of ``plot`` file
    """
    
    _logging.info('starting plot step for control file {}...'.format(
                   control_file))

    # Read the YAML control file

    control_dict = _read_control_file(control_file)

    # De-reference the information from the control file to be used in this tool

    output_prefix = control_dict['output_prefix']
    all_cubelets_dict = control_dict['cubelets']
    redshift = control_dict['redshift']

    # Fit each cubelet as requested

    for cubelet_name, cubelet_dict in all_cubelets_dict.items():
        
        cubelet_filename = '{}-{}.fits'.format(output_prefix, cubelet_name)
        line_rf_wl = cubelet_dict['line_rf_wl']
        
        if 'fittings' in cubelet_dict:
            
            for fitting_name, fitting_dict in cubelet_dict['fittings'].items():

                # Get the fit model used for fitting

                fit_model = _get_model_from_name(
                    fitting_dict['fit_model'], fitting_dict['fit_model_params'])

                # Get the names of the files which contents will be displayed
                
                fittings_filename = '{}-{}-fitting-{}.hdf5'.format(
                    output_prefix, cubelet_name, fitting_name)
                best_fit_filename = '{}-{}-fitting-{}.fits'.format(
                    output_prefix, cubelet_name, fitting_name)

                # Set a title for the figure
                
                suptitle = '{} - {} - {}'.format(
                    output_prefix, cubelet_name, fitting_name)
                
                _logging.info('plotting fits in {}/{}...'.format(
                    fittings_filename, best_fit_filename))

                # Get the AoN considered for the best fit (if available)

                try:
                    best_fit_aon = fitting_dict['best_fit_aon']
                except:
                    best_fit_aon = None

                # View the fittings
                
                _view_fits(fittings_filename, best_fit_filename, fit_model,
                           suptitle=suptitle, best_fit_aon=best_fit_aon,
                           type_params=type_params, line_rf_wl=line_rf_wl,
                           interactive=interactive, vperc=vperc,
                           add_v_sys=add_v_sys, redshift=redshift)


def _main(opts=None):

    # Parse command line switches
    
    if opts is None:
        args = _sys.argv[1:]
    
        parser = get_parser()
        opts = parser.parse_args(args)
    
    # Start off the logging and silent warnings (if requested)
    
    _set_logging_config(opts.log_level)
    _set_warning_config(opts.lib_warnings)
    
    # Call the main function

    get_plot(opts.control_file, type_params=opts.type_params,
             vperc=opts.vperc, add_v_sys=opts.add_v_sys,
             interactive=opts.interactive)


if __name__ == '__main__':
    _sys.exit(_main())

