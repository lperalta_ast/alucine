#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ALUCINE: A set of tools for emission line fitting of astronomical IFU data
# Copyright (C) 2022  Luis Peralta de Arriba
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Colour maps imitating those used by SAURON survey
"""


import numpy as _np
from matplotlib.colors import ListedColormap as _ListedColormap
from matplotlib.colors import LinearSegmentedColormap as _LinearSegmentedColormap


def _get_white():
    
    white = _np.array([1.0, 1.0, 1.0, 1.0])
    
    return white


def _get_purple():
    
    purple = _np.array([0.6, 0.0, 0.4, 1.0])
    
    return purple


def _get_brown():
    
    brown = _np.array([0.625, 0.3203125, 0.17578125, 1.0])
    
    return brown


def _create_rgb_dict_from_lists(x_array, rgb_lists):
    
    rgb_dict = {color: [(x, rgb_lists[color][i],
                            rgb_lists[color][i])
                        for i, x in enumerate(x_array)]
                for color in rgb_lists}

    return rgb_dict


def _create_reverse_rgb_dict_from_lists(x_array, rgb_lists):
    
    rgb_dict = {color: [(x, rgb_lists[color][::-1][i],
                            rgb_lists[color][::-1][i])
                        for i, x in enumerate(x_array)]
                for color in rgb_lists}

    return rgb_dict


def _build_sauron_and_nouras_cmaps():

    # Get the x positions and their RGB values

    sauron_x_array = ((_np.array(
                         [1, 43.5, 86, (86 + 20), (128.5 - 10), 128.5,
                          (128.5 + 10), (171 - 20), 171, 213.5, 256]) - 1)
                      / (256 - 1))

    sauron_rgb_lists = {
        'red':   [0.0, 0.0, 0.4,  0.5, 0.3, 0.0, 0.7, 1.0, 1.0,  1.0, 0.9],
        'green': [0.0, 0.0, 0.85, 1.0, 1.0, 0.9, 1.0, 1.0, 0.85, 0.0, 0.9],
        'blue':  [0.0, 1.0, 1.0,  1.0, 0.7, 0.0, 0.0, 0.0, 0.0,  0.0, 0.9]
    }

    # Create the dictionaries needed for the creation of the colormaps

    sauron_dict = _create_rgb_dict_from_lists(sauron_x_array,
                                              sauron_rgb_lists)

    noruas_dict = _create_reverse_rgb_dict_from_lists(sauron_x_array,
                                                      sauron_rgb_lists)

    # Create the color maps

    sauron_cmap = _LinearSegmentedColormap('sauron', sauron_dict)
    noruas_cmap = _LinearSegmentedColormap('noruas', noruas_dict)
    
    return sauron_cmap, noruas_cmap


def _build_white_sauron_and_nouras_cmaps(sauron_cmap, noruas_cmap):

    white = _get_white()

    sauron_white_colors = sauron_cmap(_np.linspace(0, 1, 256))
    sauron_white_colors[-1:, :] = white
    sauron_white_cmap = _ListedColormap(sauron_white_colors,
                                        name='sauron_white')

    noruas_white_colors = noruas_cmap(_np.linspace(0, 1, 256))
    noruas_white_colors[:1, :] = white
    noruas_white_cmap = _ListedColormap(noruas_white_colors,
                                        name='noruas_white')
    
    return sauron_white_cmap, noruas_white_cmap


def _build_purple_sauron_and_nouras_cmaps(sauron_cmap, noruas_cmap):

    purple = _get_purple()

    sauron_purple_colors = sauron_cmap(_np.linspace(0, 1, 256))
    sauron_purple_colors[-1:, :] = purple
    sauron_purple_cmap = _ListedColormap(sauron_purple_colors,
                                         name='sauron_purple')

    noruas_purple_colors = noruas_cmap(_np.linspace(0, 1, 256))
    noruas_purple_colors[:1, :] = purple
    noruas_purple_cmap = _ListedColormap(noruas_purple_colors,
                                         name='noruas_purple')
    
    return sauron_purple_cmap, noruas_purple_cmap


def _build_brown_sauron_and_nouras_cmaps(sauron_cmap, noruas_cmap):

    brown = _get_brown()

    sauron_brown_colors = sauron_cmap(_np.linspace(0, 1, 256))
    sauron_brown_colors[:1, :] = brown
    sauron_brown_cmap = _ListedColormap(sauron_brown_colors, name='sauron_brown')

    noruas_brown_colors = noruas_cmap(_np.linspace(0, 1, 256))
    noruas_brown_colors[-1:, :] = brown
    noruas_brown_cmap = _ListedColormap(noruas_brown_colors, name='noruas_brown')
    
    return sauron_brown_cmap, noruas_brown_cmap


def _build_white_brown_sauron_and_nouras_cmaps(sauron_cmap, noruas_cmap):

    white = _get_white()
    brown = _get_brown()

    sauron_white_brown_colors = sauron_cmap(_np.linspace(0, 1, 256))
    sauron_white_brown_colors[:1, :] = brown
    sauron_white_brown_colors[-1:, :] = white
    sauron_white_brown_cmap = _ListedColormap(sauron_white_brown_colors,
                                             name='sauron_white_brown')

    noruas_white_brown_colors = noruas_cmap(_np.linspace(0, 1, 256))
    noruas_white_brown_colors[:1, :] = white
    noruas_white_brown_colors[-1:, :] = brown
    noruas_white_brown_cmap = _ListedColormap(noruas_white_brown_colors,
                                             name='noruas_white_brown')
    
    return sauron_white_brown_cmap, noruas_white_brown_cmap


def _build_purple_brown_sauron_and_nouras_cmaps(sauron_cmap, noruas_cmap):

    purple = _get_purple()
    brown = _get_brown()

    sauron_purple_brown_colors = sauron_cmap(_np.linspace(0, 1, 256))
    sauron_purple_brown_colors[:1, :] = brown
    sauron_purple_brown_colors[-1:, :] = purple
    sauron_purple_brown_cmap = _ListedColormap(sauron_purple_brown_colors,
                                              name='sauron_purple_brown')

    noruas_purple_brown_colors = noruas_cmap(_np.linspace(0, 1, 256))
    noruas_purple_brown_colors[:1, :] = purple
    noruas_purple_brown_colors[-1:, :] = brown
    noruas_purple_brown_cmap = _ListedColormap(noruas_purple_brown_colors,
                                              name='noruas_purple_brown')
    
    return sauron_purple_brown_cmap, noruas_purple_brown_cmap


sauron_cmap, noruas_cmap = _build_sauron_and_nouras_cmaps()

sauron_cmap = sauron_cmap
"""
sauron_cmap
"""

sauron_white_cmap, noruas_white_cmap = (
    _build_white_sauron_and_nouras_cmaps(sauron_cmap, noruas_cmap))

sauron_purple_cmap, noruas_purple_cmap = (
    _build_purple_sauron_and_nouras_cmaps(sauron_cmap, noruas_cmap))

sauron_brown_cmap, noruas_brown_cmap = (
    _build_brown_sauron_and_nouras_cmaps(sauron_cmap, noruas_cmap))

sauron_white_brown_cmap, noruas_white_brown_cmap = (
    _build_white_brown_sauron_and_nouras_cmaps(sauron_cmap, noruas_cmap))

sauron_purple_brown_cmap, noruas_purple_brown_cmap = (
    _build_purple_brown_sauron_and_nouras_cmaps(sauron_cmap, noruas_cmap))


if __name__ == '__main__':

    import matplotlib.pyplot as _plt

    cmap_list = [
        sauron_cmap, noruas_cmap,
        sauron_white_cmap, noruas_white_cmap,
        sauron_purple_cmap, noruas_purple_cmap,
        sauron_brown_cmap, noruas_brown_cmap,
        sauron_white_brown_cmap, noruas_white_brown_cmap,
        sauron_purple_brown_cmap, noruas_purple_brown_cmap]

    gradient_row = _np.linspace(0, 1, 256)
    gradient_image = _np.vstack((gradient_row, gradient_row))

    fig, ax_vect = _plt.subplots(nrows=len(cmap_list) + 1, figsize=(6.4, 3.4))
    
    fig.subplots_adjust(top=0.9, bottom=0.04, left=0.2, right=0.99)
    
    for ax, cmap in zip(ax_vect, cmap_list):
        ax.imshow(gradient_image, aspect='auto', cmap=cmap)
        ax.text(-0.01, 0.5, cmap.name, va='center', ha='right', fontsize=10,
                transform=ax.transAxes)

    for ax in ax_vect:
        ax.set_axis_off()

    _plt.show()

