#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ALUCINE: A set of tools for emission line fitting of astronomical IFU data
# Copyright (C) 2022  Luis Peralta de Arriba
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Colour maps imitating those used by MAGNUM survey
"""


import numpy as _np
from matplotlib.cm import get_cmap as _get_cmap
from matplotlib.colors import ListedColormap as _ListedColormap


def _build_magnum_cmaps():

    # 3 * 321 + 4 * 9 + 2 * 1 = 1001 
    num_points = 321
    x_array = _np.linspace(0.15, 1.0, num_points)

    blue_magnum_colors = _get_cmap('Blues')(x_array)
    green_magnum_colors = _get_cmap('Greens')(x_array)
    red_magnum_colors = _get_cmap('Reds')(x_array)

    blue_magnum_cmap = _ListedColormap(blue_magnum_colors,
                                       name='blue_magnum')
    green_magnum_cmap = _ListedColormap(green_magnum_colors,
                                        name='green_magnum')
    red_magnum_cmap = _ListedColormap(red_magnum_colors,
                                      name='red_magnum')

    num_edge_points = 9
    blue_high_colors = _np.tile(blue_magnum_colors[-1, :], (num_edge_points, 1))
    green_low_colors = _np.tile(green_magnum_colors[0, :], (num_edge_points, 1))
    green_high_colors = _np.tile(green_magnum_colors[-1, :], (num_edge_points, 1))
    red_low_colors = _np.tile(red_magnum_colors[0, :], (num_edge_points, 1))
    white_color = _np.array([[1.0, 1.0, 1.0, 1.0]])

    x_array = _np.linspace(0.0, 1.0, num_points)

    magnum_colors = _np.vstack(
        [blue_magnum_cmap(x_array), blue_high_colors,
         white_color,
         green_low_colors, green_magnum_cmap(x_array), green_high_colors,
         white_color, red_low_colors, red_magnum_cmap(x_array)])

    magnum_cmap = _ListedColormap(magnum_colors, name='magnum')
    
    return magnum_cmap, blue_magnum_cmap, green_magnum_cmap, red_magnum_cmap


def category_value_to_magnum_value(category, value):
    """
    category_value_to_magnum_value
    """

    if category in [1, 2, 3]:
        if category == 1:
            min_value = 0.000   #    0
            max_value = 0.328   #  329        0.305   #  306
        elif category == 2:
            min_value = 0.332   #  331        0.315   #  314
            max_value = 0.669   #  668        0.685   #  686
        elif category == 3:
            min_value = 0.672   #  671        0.695   #  694
            max_value = 1.000   # 1000
    
        magnum_value = min_value + value * (max_value - min_value)
    else:
        magnum_value = _np.nan
    
    return magnum_value


magnum_cmap, blue_magnum_cmap, green_magnum_cmap, red_magnum_cmap = (
    _build_magnum_cmaps())


if __name__ == '__main__':

    import matplotlib.pyplot as _plt

    cmap_list = [
        blue_magnum_cmap, green_magnum_cmap, red_magnum_cmap,
        magnum_cmap]

    gradient_row = _np.linspace(0, 1, 256)
    gradient_image = _np.vstack((gradient_row, gradient_row))
    gradient_image = _np.tile(_np.linspace(0, 1, 1001), (2, 1))

    fig, ax_vect = _plt.subplots(nrows=len(cmap_list) + 1 + 3,
                                 figsize=(6.4, 3.4))
    
    fig.subplots_adjust(top=0.9, bottom=0.04, left=0.2, right=0.99)
    
    for ax, cmap in zip(ax_vect, cmap_list):
        ax.imshow(gradient_image, aspect='auto', cmap=cmap)
        ax.text(-0.01, 0.5, cmap.name,
                va='center', ha='right', fontsize=10, transform=ax.transAxes)
    
    category_ax_vect = ax_vect[len(cmap_list):-1]
    
    for category, ax in enumerate(category_ax_vect, start=1):
        category_gradient_image = category_value_to_magnum_value(
                                      category, gradient_image)
        ax.imshow(category_gradient_image, aspect='auto', cmap=magnum_cmap,
                  vmin=0.0, vmax=1.0)
        ax.text(-0.01, 0.5, 'magnum - category {}'.format(category),
                va='center', ha='right', fontsize=10, transform=ax.transAxes)

    for ax in ax_vect:
        ax.set_axis_off()

    _plt.show()

