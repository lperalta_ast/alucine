#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ALUCINE: A set of tools for emission line fitting of astronomical IFU data
# Copyright (C) 2022  Luis Peralta de Arriba
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
``plotting`` file: Utils related with plotting
"""


import numpy as _np


class ClickZoomFigUpdater:
    """
    ClickZoomFigUpdater
    """
    
    def __init__(self, fig, click_zoom_vect_ax, updated_vect_ax,
                 updater_function, data, filename_pattern=None):
        
        self.fig = fig
        self.click_zoom_vect_ax = click_zoom_vect_ax
        self.updated_vect_ax = updated_vect_ax
        self.updater_function = updater_function
        self.data = data
        self.filename_pattern = filename_pattern
        
        self._share_axes()
        
        self.x = int(_np.mean(self.click_zoom_vect_ax[0].get_xlim()))
        self.y = int(_np.mean(self.click_zoom_vect_ax[0].get_ylim()))
        self._update_axes()
        
        self.cid = fig.canvas.mpl_connect('button_press_event', self)
    
    def __call__(self, event):
        
        for ax in self.click_zoom_vect_ax:
            
            if event.inaxes == ax:
                
                if event.button == 1:
                    self.x = int(_np.round(event.xdata))
                    self.y = int(_np.round(event.ydata))
                    self._update_axes()
                elif event.button == 3:
                    self._savefig()
                
                break
    
    def _share_axes(self):
    
        ref_ax = self.click_zoom_vect_ax[0]
        
        for i, ax in enumerate(self.click_zoom_vect_ax):
        
            if i > 0:
                
                ref_ax.get_shared_x_axes().join(ref_ax, ax)
                ref_ax.get_shared_y_axes().join(ref_ax, ax)
        
        ref_ax.autoscale()
    
        ref_ax = self.updated_vect_ax[0]
        
        for i, ax in enumerate(self.updated_vect_ax):
        
            if i > 0:
                
                ref_ax.get_shared_x_axes().join(ref_ax, ax)
                ref_ax.get_shared_y_axes().join(ref_ax, ax)
        
        ref_ax.autoscale()
    
    def _update_axes(self):
        
        self.updater_function(
            self.fig, self.updated_vect_ax, self.data, self.x, self.y)
    
    def _savefig(self):
        
        if self.filename_pattern is not None:
            
            suffix = '{}_{}'.format(self.x, self.y)
            
            self.fig.savefig(self.filename_pattern.format(suffix))

