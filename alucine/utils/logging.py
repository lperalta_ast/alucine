#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ALUCINE: A set of tools for emission line fitting of astronomical IFU data
# Copyright (C) 2022  Luis Peralta de Arriba
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
``logging`` file: Utils related with logging
"""


import logging as _logging
from warnings import filterwarnings as _filterwarnings

from astropy.utils.exceptions import AstropyWarning as _AstropyWarning
from spectral_cube.utils      import SpectralCubeWarning as _SpectralCubeWarning


def add_logging_to_parser(parser):
    
    parser.add_argument('--log_level', default='info',
                        choices=['debug', 'info', 'warning', 'error'],
                        help=('the level for the logging messages '+
                              '(default: %(default)s)'))
    
    group_lib_warnings = parser.add_mutually_exclusive_group()
    group_lib_warnings.add_argument(
        '--show_lib_warnings', dest='lib_warnings', action='store_true',
        help='show the warnings of the imported libraries')
    group_lib_warnings.add_argument(
        '--hide_lib_warnings', dest='lib_warnings', action='store_false',
        help='hide the warnings of the imported libraries')
    group_lib_warnings.set_defaults(lib_warnings=False)


def set_logging_config(log_level):
    
    _logging.basicConfig(level=getattr(_logging, log_level.upper()),
                         format='[%(levelname)s] %(asctime)s: %(message)s')


def set_warning_config(show_lib_warnings):
    
    if show_lib_warnings is False:
        for category in [RuntimeWarning, _AstropyWarning, _SpectralCubeWarning]:
            _filterwarnings(action='ignore', category=category, append=True)

