#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ALUCINE: A set of tools for emission line fitting of astronomical IFU data
# Copyright (C) 2022  Luis Peralta de Arriba
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
``io`` file: Utils related with input/output operations
"""

from re import match as _match

import yaml as _yaml
import h5py as _h5py
import numpy as _np
from astropy.io import fits as _fits
from spectral_cube import SpectralCube as _SpectralCube

from .wcs import overwrite_wcs


def read_control_file(control_file):
    """
    Read an ALUCINE control file
    """

    with open(control_file, 'r') as f:
        control_dict = _yaml.load(f, Loader=_yaml.SafeLoader)

    return control_dict


def read_cube(filename, alt_wcs=None):
    """
    Read a FITS cube
    """

    # Open the input file

    with _fits.open(filename, mode='readonly') as hdu_list:

        # We assume that the third axis is the spectral axis

        wl_ax_index = 3
        assert hdu_list[0].header['CTYPE{}'.format(wl_ax_index)] in ['AWAV', 'WAVE']

        # Overwrite the main WCS with the alternative if requested
        
        if alt_wcs is not None:
            overwrite_wcs(hdu_list, wl_ax_index, alt_wcs)
        
        # We want that CUNIT3 to be Anstrom
        
        if hdu_list[0].header['CUNIT{}'.format(wl_ax_index)] == 'MICRON':
            hdu_list[0].header['CRVAL{}'.format(wl_ax_index)] *= 10000
            hdu_list[0].header['CDELT{}'.format(wl_ax_index)] *= 10000
            hdu_list[0].header['CUNIT{}'.format(wl_ax_index)] = 'Angstrom'
        
        assert hdu_list[0].header['CUNIT{}'.format(wl_ax_index)] == 'Angstrom'

        # Create a cube

        cube = _SpectralCube.read(hdu_list, hdu=0)

    return cube


def write_multifitting_to_hdf5(filename, multifitting, root_attrs=None):
    """
    Write multiffing information to a HDF5 file
    """
    
    with _h5py.File(filename, 'w') as f:
        
        if root_attrs is not None:
            for key, value in root_attrs.items():
                f.attrs[key] = value
        
        for spaxel_key in multifitting.keys():
            
            grp_name = str(spaxel_key)
            grp = f.create_group(grp_name)
            
            for key in multifitting[spaxel_key].keys():
                
                if type(key) != int:
                    
                    value = multifitting[spaxel_key][key]
                    
                    if type(value) != _np.ndarray:
                        grp.attrs[key] = value
                    else:
                        grp.create_dataset(key, data=value)
                    
                else:
                
                    subgrp_name = str(key)
                    subgrp = grp.create_group(subgrp_name)
                    
                    for subkey in multifitting[spaxel_key][key].keys():
                        
                        value = multifitting[spaxel_key][key][subkey]
                        
                        if type(value) != _np.ndarray:
                            subgrp.attrs[subkey] = value
                        else:
                            subgrp.create_dataset(subkey, data=value)


def _get_attr_grp_dict_from_hdf5(f, with_root_attrs=True):
    
    result = {}
    
    if with_root_attrs is True:
        for attr in f.attrs:
            result[attr] = f.attrs[attr]
    
    for grp_name in f:
        
        if _match('^[0-9]+$', grp_name):
            key = int(grp_name)
        elif _match('^\([0-9]+, [0-9]+\)$', grp_name):
            key = tuple(map(int,
                            grp_name.strip('()').replace(' ', '').split(',')))
        else:
            key = grp_name
        
        if isinstance(f[grp_name], _h5py.Group):
            value = _get_attr_grp_dict_from_hdf5(f[grp_name])
        elif isinstance(f[grp_name], _h5py.Dataset):
            value = _np.array(f[grp_name])
        else:
            value = f[grp_name]
        
        result[key] = value
    
    return result


def read_multifitting_from_hdf5(filename, with_root_attrs=True):
    """
    Read multiffing information from a HDF5 file
    """
    
    with _h5py.File(filename, 'r') as f:
        multifitting = _get_attr_grp_dict_from_hdf5(
            f, with_root_attrs=with_root_attrs)
    
    return multifitting

