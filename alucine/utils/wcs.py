#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ALUCINE: A set of tools for emission line fitting of astronomical IFU data
# Copyright (C) 2022  Luis Peralta de Arriba
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
``wcs`` file: Utils related with WCS
"""


import logging as _logging
import re as _re


def overwrite_wcs(hdu_list, wl_ax_index, alt_wcs):

    crval_kwd = 'CRVAL{}'.format(wl_ax_index)
    cdelt_kwd = 'CDELT{}'.format(wl_ax_index)

    alt_crval_kwd = 'CRVAL{}{}'.format(wl_ax_index, alt_wcs)
    alt_cdelt_kwd = 'CDELT{}{}'.format(wl_ax_index, alt_wcs)

    orig_crval = hdu_list[0].header[crval_kwd]
    orig_cdelt = hdu_list[0].header[cdelt_kwd]

    new_crval = hdu_list[0].header[alt_crval_kwd]
    new_cdelt = hdu_list[0].header[alt_cdelt_kwd]

    _logging.info(
        'WCS info will be overwritten by the alternative {}:'.format(
            alt_wcs))

    _logging.info(
        ' - {} ({}) value replaced by {} ({})'.format(
            crval_kwd, orig_crval, alt_crval_kwd, new_crval))

    hdu_list[0].header[crval_kwd] = new_crval

    _logging.info(
        ' - {} ({}) value replaced by {} ({})'.format(
            cdelt_kwd, orig_cdelt, alt_cdelt_kwd, new_cdelt))

    hdu_list[0].header[cdelt_kwd] = new_cdelt


def remove_kwds_of_third_axis(hdr):
    """
    remove_kwds_of_third_axis
    """

    assert hdr['NAXIS'] == 3

    hdr['NAXIS'] = 2
    
    for kwd in hdr:
        if _re.match('^WCSAXES[B-Z]*$', kwd):
            hdr[kwd] = 2

    kwd_regex_list = [
        '^NAXIS3$', '^CTYPE3[B-Z]*$', '^CUNIT3[B-Z]*$', '^CRVAL3[B-Z]*$',
        '^CDELT3[B-Z]*$', '^CRPIX3[B-Z]*$', '^CROTA3[B-Z]*$',
        '^CD3_[1-3][B-Z]*', '^CD[1-2]_3[B-Z]*$',
        '^PC3_[1-3][B-Z]*', '^PC[1-2]_3[B-Z]*$']
    
    kwd_list = list(hdr.keys())
    
    for kwd in kwd_list:
        if kwd not in ['COMMENT', 'HISTORY']:
            for kwd_regex in kwd_regex_list:
                if _re.match(kwd_regex, kwd):
                    hdr.remove(kwd)

