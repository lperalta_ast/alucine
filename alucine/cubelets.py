#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ALUCINE: A set of tools for emission line fitting of astronomical IFU data
# Copyright (C) 2022  Luis Peralta de Arriba
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
``cubelets`` file
"""


import logging as _logging
from argparse import ArgumentParser as _ArgumentParser

import numpy as _np
import astropy.units as _u
from scipy.optimize import curve_fit as _curve_fit
from astropy.io import fits as _fits
from spectral_cube import SpectralCube as _SpectralCube

from .utils.io import read_control_file as _read_control_file
from .utils.io import read_cube as _read_cube
from .utils.logging import add_logging_to_parser as _add_logging_to_parser
from .utils.logging import set_logging_config as _set_logging_config
from .utils.logging import set_warning_config as _set_warning_config
from .utils.wcs import remove_kwds_of_third_axis as _remove_kwds_of_third_axis


def get_parser(add_help=True):
    """
    Parser of ``cubelets`` file
    """

    parser = _ArgumentParser(add_help=add_help,
        description='ALUCINE step 2: creating cubelets')

    parser.add_argument('control_file',
                        help='a YAML file with the control instructions')
    
    _add_logging_to_parser(parser)
    
    parser.set_defaults(func=_main)

    return parser


def _get_levels_at_continuum(xdata, ydata, cont_rf_window_list=None):
        
    assert len(cont_rf_window_list) == 2
    assert (cont_rf_window_list[0][0] < cont_rf_window_list[0][1])
    assert (cont_rf_window_list[1][0] < cont_rf_window_list[1][1])
    assert (cont_rf_window_list[0][1] < cont_rf_window_list[1][0])
    
    cont_channels0 = ((xdata > cont_rf_window_list[0][0]) *
                      (xdata < cont_rf_window_list[0][1]))
    
    baseline_level0 = _np.nanmedian(ydata[cont_channels0])
    
    cont_channels1 = ((xdata > cont_rf_window_list[1][0]) *
                      (xdata < cont_rf_window_list[1][1]))
    
    baseline_level1 = _np.nanmedian(ydata[cont_channels1])
    
    return baseline_level0, baseline_level1


def _get_low_bounds(xdata, ydata, noise=None, cont_rf_window_list=None):
    
    baseline_level0, baseline_level1 = (
        _get_levels_at_continuum(
            xdata, ydata, cont_rf_window_list=cont_rf_window_list))
    
    baseline_low_bounds = _np.array([baseline_level0 - 3.0 * noise,
                                     baseline_level1 - 3.0 * noise])
    
    return baseline_low_bounds


def _get_high_bounds(xdata, ydata, noise=None, cont_rf_window_list=None):
    
    baseline_level0, baseline_level1 = (
        _get_levels_at_continuum(
            xdata, ydata, cont_rf_window_list=cont_rf_window_list))
    
    baseline_high_bounds = _np.array([baseline_level0 + 3.0 * noise,
                                      baseline_level1 + 3.0 * noise])
    
    return baseline_high_bounds


def _get_init_params(xdata, ydata, low_bounds, high_bounds):
    
    baseline_param0 = (high_bounds[0] + low_bounds[0]) / 2.0
    
    baseline_param1 = (high_bounds[1] + low_bounds[1]) / 2.0
    
    baseline_init_params = _np.array([baseline_param0, baseline_param1])
    
    return baseline_init_params


def _create_one_cubelet(cube, redshift, min_rf_wl, max_rf_wl,
                        cont_rf_window_list, cubelet_filename, cont_order=0):
    
    assert cont_order in [0, 1]

    # Get the coordinates of the cube along the spectral axis

    obs_wl_array_with_unit, _, _ = cube.world[:, 0, 0]
    
    obs_wl_array = obs_wl_array_with_unit.value
    assert obs_wl_array_with_unit.unit == _u.AA

    # Select the channels to be used to estimate the continuum and the noise

    cont_channels = _np.zeros(len(obs_wl_array), dtype=bool)

    for cont_rf_window in cont_rf_window_list:

        min_cont_obs_wl = (1.0 + redshift) * cont_rf_window[0]
        max_cont_obs_wl = (1.0 + redshift) * cont_rf_window[1]

        window_cont_channels = ((obs_wl_array > min_cont_obs_wl) *
                                (obs_wl_array < max_cont_obs_wl))

        cont_channels += window_cont_channels

    # Compute the noise in the continuum regions

    cont_noise_image = cube.with_mask(
        cont_channels[:, _np.newaxis, _np.newaxis]).std(axis=0)

    if cont_order == 0:
    
        # Create image with the continuum to be substracted for each line

        cont_level_image = cube.with_mask(
            cont_channels[:, _np.newaxis, _np.newaxis]).median(axis=0)

        # Substract the continuum images to the cube

        cont_cube = cube - cont_level_image
        
    elif cont_order == 1:
        
        # We copy the cube for cheating its structure and set it to zero
        
        cont_level_cube_array = _np.zeros(cube.shape)
        
        num_wl, num_y, num_x = cont_level_cube_array.shape
        
        ### Option A (it works!, but fits do not like the best ones)
        
        cont_obs_wl = obs_wl_array[window_cont_channels]
    
        for y in range(num_y):
            for x in range(num_x):
                cont_flux = cube.hdu.data[window_cont_channels, y, x]
                
                m, b = _np.polyfit(cont_obs_wl, cont_flux, 1)

                cont_level_cube_array[:, y, x] = m * obs_wl_array + b
        
        ### End of option A
        
#        # Option B (it does NOT works, but it should be better)
#        
#        cont_obs_wl = obs_wl_array[window_cont_channels]
#        cont_obs_wl = _np.array(cont_obs_wl.data)
#        
#        fit_function = (lambda xdata, *params: 
#                        _np.zeros(len(xdata)) +
#                        params[0] + (params[1] - params[0]) *
#                        (xdata - cont_obs_wl[0]) / (cont_obs_wl[-1] - cont_obs_wl[0]))
#    
#        for y in range(num_y):
#            for x in range(num_x):
#                
#                noise = cont_noise_image[y, x].value
#            
#                cont_flux = cube.hdu.data[window_cont_channels, y, x]
##                cont_flux = _np.array(cont_flux.data)
#                
#                low_bounds = _get_low_bounds(cont_obs_wl, cont_flux,
#                    noise=noise, cont_rf_window_list=cont_rf_window_list)
#                high_bounds = _get_high_bounds(cont_obs_wl, cont_flux,
#                    noise=noise, cont_rf_window_list=cont_rf_window_list)

#                init_params = _get_init_params(cont_obs_wl, cont_flux,
#                                               low_bounds, high_bounds)
#                
#                bounds = _np.array([low_bounds, high_bounds])
#                
#                if _np.count_nonzero(_np.isnan(init_params)) == 0:

#                    popt, pcov = _curve_fit(
#                        fit_function, cont_obs_wl, cont_flux,
#                        p0=tuple(init_params), bounds=bounds)

#                    fit_params = popt
#                    cont_level_cube_array[:, y, x] = fit_function(
#                        obs_wl_array.data, *fit_params)
#                
#                else:
#                    
#                    cont_level_cube_array[:, y, x] = 0.0
#        
#        ### End of option B

        cont_level_cube = _SpectralCube(cont_level_cube_array,
                                        cube.wcs, meta=cube.meta)

        # Substract the continuum images to the cube
        
        cont_cube = cube - cont_level_cube
        
    else:
            raise NotImplementedError

    # Create the cubelet in the requested range

    min_cubelet_obs_wl = (1.0 + redshift) * min_rf_wl
    max_cubelet_obs_wl = (1.0 + redshift) * max_rf_wl

    cubelet = cont_cube.spectral_slab(min_cubelet_obs_wl * _u.AA,
                                      max_cubelet_obs_wl * _u.AA)

    # Write the cubelet to a FITS file

    cubelet.write(cubelet_filename, format='fits', overwrite=True)
    
    # Get the images with the continuum level(s)

    if cont_order == 0:
    
        pass
        
    elif cont_order == 1:

        cont_level_cubelet = cont_level_cube.spectral_slab(
            min_cubelet_obs_wl * _u.AA, max_cubelet_obs_wl * _u.AA)
        
        # Create image with the continuum to be substracted for each line
        
        cont_level_image0 = cont_level_cubelet.hdu.data[0, :, :]
        cont_level_image1 = cont_level_cubelet.hdu.data[-1, :, :]
        
    else:
            raise NotImplementedError
    
    # Append extensions with extra infomation
    
    with _fits.open(cubelet_filename, mode='update') as hdu_list:
    
        # Fix the WCS keyword for the cubelet
    
#        new_crval3 = cubelet.world[:, 0, 0][0][0].value
#        _logging.critical('{}: {} -> {}'.format(
#            cubelet_filename, hdu_list[0].header['CRVAL3'], new_crval3))
#        _logging.critical('{}: {} -> {}'.format(
#            cubelet_filename, hdu_list[0].header['CRVAL3B'], new_crval3))
#        
#        hdu_list[0].header['CRVAL3'] = new_crval3
#        hdu_list[0].header['CRVAL3B'] = new_crval3
        #_logging.critical(hdu_list[0].header['CRVAL3'])
        
        # Prepare the header for the other extensions
    
        hdr = hdu_list[0].header.copy()
        _remove_kwds_of_third_axis(hdr)
        
        # Add one extension with the noise at the continuum
        
        cont_noise_hdu = _fits.ImageHDU(data=cont_noise_image.array, header=hdr,
                                        name='CONT_NOISE')
        hdu_list.append(cont_noise_hdu)
        
        # Add extension(s) with the continuum level(s)

        if cont_order == 0:
        
            cont_level_hdu = _fits.ImageHDU(data=cont_level_image.array,
                                            header=hdr, name='CONT_LEVEL')
            hdu_list.append(cont_level_hdu)
            
        elif cont_order == 1:
        
            cont_level_hdu = _fits.ImageHDU(data=cont_level_image0,
                                            header=hdr, name='CONT_LEVEL0')
            hdu_list.append(cont_level_hdu)
        
            cont_level_hdu = _fits.ImageHDU(data=cont_level_image1,
                                            header=hdr, name='CONT_LEVEL1')
            hdu_list.append(cont_level_hdu)
        
        else:
                raise NotImplementedError


def get_cubelets(control_file):
    """
    Main function of ``cubelets`` file
    """

    _logging.info('starting cubelets step for control file {}...'.format(
                   control_file))

    # Read the YAML control file

    control_dict = _read_control_file(control_file)

    # De-reference the information from the control file to be used in this tool

    cube_filename = control_dict['cube_filename']
    output_prefix = control_dict['output_prefix']
    redshift = control_dict['redshift']
    alt_wcs = control_dict['alt_wcs']
    all_cubelets_dict = control_dict['cubelets']

    # Read the cube

    cube = _read_cube(cube_filename, alt_wcs=alt_wcs)
    cube.allow_huge_operations=True

    # Create the requested cubelets

    for cubelet_name, cubelet_dict in all_cubelets_dict.items():

        cubelet_filename = '{}-{}.fits'.format(output_prefix, cubelet_name)

        _logging.info('creating cubelet {}...'.format(cubelet_filename))
        
        min_rf_wl = cubelet_dict['min_rf_wl']
        max_rf_wl = cubelet_dict['max_rf_wl']
        cont_rf_window_list = cubelet_dict['cont_rf_window_list']
        
        if 'cont_order' in cubelet_dict.keys():
            cont_order = cubelet_dict['cont_order']
        else:
            cont_order = 0
            _logging.info('assuming cont_order=0 for cubelet {}...'.format(
                cubelet_filename))

        _create_one_cubelet(cube, redshift, min_rf_wl, max_rf_wl,
                            cont_rf_window_list, cubelet_filename,
                            cont_order=cont_order)


def _main(opts=None):

    # Parse command line switches
    
    if opts is None:
        args = _sys.argv[1:]
    
        parser = get_parser()
        opts = parser.parse_args(args)
    
    # Start off the logging and silent warnings (if requested)
    
    _set_logging_config(opts.log_level)
    _set_warning_config(opts.lib_warnings)
    
    # Call the main function
    
    get_cubelets(opts.control_file)


if __name__ == '__main__':
    _sys.exit(_main())

