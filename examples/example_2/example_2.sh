# Create a pseudo-cube with the extractions from the given apertures
alucine tools ap-extract ngc7172-cube.fits apertures_2.txt --out_filename ngc7172-extract_cube.fits

# Run ALUCINE (all steps at ones)
alucine full control_file_2.yaml

# Call the plot command to inspect the results interatively
alucine plot control_file_2.yaml

