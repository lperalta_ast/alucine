import matplotlib.pyplot as plt

from alucine.data_products.fitting import Fitting


################################################################################

# The contents of the FITS file at a glance

# Open the FITS file
filename = 'ngc7172-halpha_nii-fitting-2_components_2aon.fits'
fitting = Fitting(filename)

################################################################################

# Plotting one spaxel, its fitting and their components

# Get the rest-frame wavelength array

rf_wl = fitting.get_rf_wl()

# Get the spectrum of one spaxel, its fitting and their components

spaxel_i, spaxel_j = 15, 18

spaxel_spec = fitting.get_spaxel_spec(spaxel_i, spaxel_j)
spaxel_fit_spec = fitting.get_spaxel_fit_spec(spaxel_i, spaxel_j)
spaxel_component_list = fitting.get_spaxel_component_list(spaxel_i, spaxel_j)

# Plot the spectrum and its fitting for one spaxel

fig, ax = plt.subplots()

ax.set_title('Data and fit at location ({}, {})'.format(spaxel_i, spaxel_j))

ax.plot(rf_wl, spaxel_spec, '-k', label='Data')
ax.plot(rf_wl, spaxel_fit_spec, '-r', label='Fit')

for i, spaxel_component in enumerate(spaxel_component_list, 1):
    ax.plot(rf_wl, spaxel_component, ls='--', label='Component {}'.format(i))

ax.legend()

plt.show()


#################################################################################

## Get the cube with the parameters used for fitting each spaxel

print('Maximum number of components: {}'.format(
    fitting.get_max_num_components()))

print('Name of scientific parameters of each component:')
for i, name_sci_param in enumerate(fitting.get_component_name_sci_params()):
    print('  {}: {}'.format(i, name_sci_param))

# Get the velocity and the velocity dispersion for the components 1 and 2

v1_image = fitting.get_sci_param_image(1, 1)
v2_image = fitting.get_sci_param_image(2, 1)

sigma1_image = fitting.get_sci_param_image(1, 2)
sigma2_image = fitting.get_sci_param_image(2, 2)

## Read the velocity dispersions from the sci_params

fig, vect_ax = plt.subplots(nrows=2, ncols=2)

vect_ax[0, 0].set_title(r'$v_{H \alpha 1}$ (km s$^{-1}$)')
vect_ax[0, 0].imshow(v1_image, origin='lower')

vect_ax[0, 1].set_title(r'$v_{H \alpha 2}$ (km s$^{-1}$')
vect_ax[0, 1].imshow(v2_image, origin='lower')

vect_ax[1, 0].set_title(r'$\sigma_{H \alpha 1}$ (km s$^{-1}$)')
vect_ax[1, 0].imshow(sigma1_image, origin='lower')

vect_ax[1, 1].set_title(r'$\sigma_{H \alpha 2}$ (km s$^{-1}$')
vect_ax[1, 1].imshow(sigma2_image, origin='lower')

plt.show()

