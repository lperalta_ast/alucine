.. ALUCINE documentation master file, created by
   sphinx-quickstart on Wed Dec 29 20:26:46 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ALUCINE's documentation!
===================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:
   
   introduction
   installation
   running
   control_file
   models
   data_products
   tools
   examples

.. toctree::
   :maxdepth: 1
   
   command_line
   api/modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

