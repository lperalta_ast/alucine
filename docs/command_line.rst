Reference / Command line
========================

.. argparse::
   :module: alucine.__main__
   :func: get_parser
   :prog: alucine
   :nodefaultconst:

