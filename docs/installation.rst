Installation
============


The Dependencies
----------------

Before proceeding to install ALUCINE, some dependencies should be available in
your computer.

The mandatory dependencies are an interpreter of the programming language
`Python <https://www.python.org/>`_, together with the following packages:

- `PyYAML <https://pypi.org/project/PyYAML/>`_
- `NumPy <https://pypi.org/project/numpy/>`_
- `h5py <https://pypi.org/project/h5py/>`_
- `SciPy <https://pypi.org/project/scipy/>`_
- `Matplotlib <https://pypi.org/project/matplotlib/>`_
- `Astropy <https://pypi.org/project/astropy/>`_
- `spectral-cube <https://pypi.org/project/spectral-cube/>`_

In addition, if you are going to install ALUCINE from the sources (currently it
is the only way), `Git <https://git-scm.com/>`_ is a highly recommended
dependency (because the version numbering of the code is managed by this version
control system).

For generating the documentation, `Sphinx <https://pypi.org/project/Sphinx/>`_
and its extension `sphinx-argparse <https://pypi.org/project/sphinx-argparse/>`_
are required. This will be enough to produce a HTML website with the
documentation. If you are interested in getting the documentation in PDF format,
a `LaTeX distribution <https://www.latex-project.org/>`_, including `Latexmk
<https://ctan.org/pkg/latexmk>`_, will be needed.


Getting the Dependencies in Debian and Ubuntu GNU/Linux
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Satisfying all the dependencies of ALUCINE in a Debian or Ubuntu GNU/Linux
distribution is easy, as it only requires to install the following packages:

- ``python3``
- ``python3-pip``
- ``python3-yaml``
- ``python3-numpy``
- ``python3-h5py``
- ``python3-scipy``
- ``python3-matplotlib``
- ``python3-astropy``
- ``python3-spectral-cube``
- ``git``
- ``python3-sphinx``
- ``python3-sphinx-argparse``
- ``texlive-latex-extra``
- ``latexmk``


Getting the Dependencies with Conda
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you are using `Conda <https://conda.io/>`_ as your package manager, you can
run these commands for creating an environment with the dependencies::

    # Create a new Conda environment named alucine
    conda create --name alucine python=3.8.10

    # Activate the Conda environment 'alucine'
    conda activate alucine

    # Install Git
    conda install -c anaconda git

    # Install the mandatory Python packages
    pip install pyyaml==5.3.1
    pip install numpy==1.17.4
    pip install h5py==2.10.0
    pip install scipy==1.3.3
    pip install matplotlib==3.1.2
    pip install astropy==4.0
    pip install spectral_cube==0.4.5

    # Install the Python packages needed for generating the documentation
    pip install sphinx==1.8.5
    pip install sphinx-argparse==0.2.2

The versions are thought to match the those available in the packages
distributed in Ubuntu 20.04 LTS, which is the GNU/Linux distribution used for
the development of ALUCINE.

Once the Conda environment has been created, you will only need to remember the
following three commands:

 - Activate the Conda environment named `alucine`::
      
      conda activate alucine
  
 - Deactivate the current Conda environment::

      conda deactivate
      
 - Delete the Conda environment named `alucine`::

      conda remove --name alucine --all


Getting the Sources
-------------------

The recommended way of getting the sources is cloning the `ALUCINE Git
repository <https://gitlab.com/lperalta_ast/alucine>`_ available at `GitLab
<https://gitlab.com/>`_. We recommend getting the sources using Git because the
version numbering of the code is managed using this version control system.

This can be performed using the following command::

    git clone git@gitlab.com:lperalta_ast/alucine.git

or alternatively::

    git clone https://gitlab.com/lperalta_ast/alucine.git

The first option requires to have a SSH key added to your GitLab account [#]_,
while the second option will ask you to enter your GitLab credentials manually.

If you would have cloned the repository previously, you could simply pull the
latest version of the code using the following command inside the main directory
``alucine``::

    git pull


Installing the Package
----------------------

For installing the Python package locally (for the current user), you can
execute inside the main directory ``alucine``::
 
    pip install .

Alternatively, if you are interested in installing the Python package globally
(for all users), you can run::

    sudo pip install .

This second option should **not** be chosen for installations with Conda.


Adding the Script to the Shell Path
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you installed the package locally, it **could** be needed add a directory to
the path of command shell (for example, Conda users will **not** need to do it).

If you are using `Bash <https://www.gnu.org/software/bash/>`_, this can be
performed adding the following line to ``~/.bashrc``::

    export PATH="/home/$USER/.local/bin:$PATH"


Uninstalling the Package
------------------------

If the package was installed locally, you need to run this comand::

    pip uninstall alucine

If the package was installed globally, the following command will do the job::

    sudo pip uninstall alucine

----

.. [#] There is a page in the GitLab documentation explaining how to `use SSH
       keys to communicate with GitLab <https://docs.gitlab.com/ee/ssh/>`_.

